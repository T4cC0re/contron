let allCharacteristicCallbacks = [];

/**
 This can be utilized by user scripts to create a promise from a callback function.
 */
const promisify = (fn) => {
    return (...args) => {
        return new Promise((resolve, reject) => {
            fn(...args, (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        });
    };
}

async function broadcastUpdateCharacteristic(service, characteristicType) {
    try {
        FilterSecurityEvent(service, characteristicType);
        FilterMotionEvent(service, characteristicType, service.characteristics[characteristicType]);
    } catch (err) {
        console.log(`ERR: broadcastUpdateCharacteristic: ${error}`);
    }
    
    for (let desc of allCharacteristicCallbacks) {
        if (!desc) {
            continue;
        }
        try {
            if (!desc.names || (desc.names && desc.names.includes(service.accessory.name))) {
                if (!desc.roomsUUIDs || (desc.roomsUUIDs && desc.roomsUUIDs.includes(service.accessory.roomUUID))) {
                    if (!desc.characteristics || (desc.characteristics && desc.characteristics.includes(characteristicType))) {
//                        console.log(`descriptor matches characteristic ${characteristicType}`);
                        desc.callback(service, characteristicType);
                    }
                }
            }
        } catch (error){
            console.log(`ERR: subscription target failed: ${error}`);
        }
    }
}
// END Called from Swift

// App logic
function subscribe(names, roomsUUIDs, characteristics, callback) {
    allCharacteristicCallbacks.push({names: names || null, roomsUUIDs: roomsUUIDs || null, characteristics: characteristics || null, callback:callback});
}

// Easy access features
function FilterSecurityEvent(service, characteristicType) {
    switch (characteristicType) {
        case "CurrentLockMechanismState":
        case "TargetLockMechanismState":
        case "CurrentDoorState":
        case "TargetDoorState":
        case "CurrentSecuritySystemState":
        case "TargetSecuritySystemState":
        case "ContactState":
        case "LockManagementAutoSecureTimeout":
        case "LockPhysicalControls":
        case "SmokeDetected":
        case "StatusFault":
        case "StatusJammed":
        case "StatusTampered":
            try {
                console.log(`FilterSecurityEvent: ${characteristicType}@${service}`);
                userSecurityEvent(service, characteristicType);
            } catch (e) {console.log(`ERR userSecurityEvent: ${e}`)}
            break;
        default:
            break;
    }
}

function FilterMotionEvent(service, characteristicType, value) {
    switch (characteristicType) {
        case "MotionDetected":
            try {
                if (value) {
                    console.log(`FilterMotionEvent: ${characteristicType}@${service}`);
                    userMotionEvent(service);
                }
            } catch (e) {console.log(`ERR userMotionEvent: ${e}`)}
            break;
        default:
            break;
    }
}
