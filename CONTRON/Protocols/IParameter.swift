//
//  IParameter.swift
//  IParameter
//
//  Created by Hendrik Meyer on 05.09.21.
//  Copyright © 2021 Hendrik Meyer. All rights reserved.
//

import Foundation

protocol IParameter: ObservableObject {
    var defaultValue: Any { get }
    var displayName: String { get }
    var minVal: Double? { get }
    var maxVal: Double? { get }
    var step: Double? { get }
    var value: Any { get set }
    var validValues: [NSNumber]? { get }
    var icon: String { get }
    var isReadable: Bool { get }
    var isWritable: Bool { get }
    var isHidden: Bool { get }
    var format: String? { get } // One of HMCharacteristicMetadataFormat* or nil
}
