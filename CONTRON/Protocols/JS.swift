//
//  JS.swift
//  CONTRON
//
//  Created by Hendrik Meyer on 11.08.21.
//  Copyright © 2021 Hendrik Meyer. All rights reserved.
//

import Foundation
import JavaScriptCore

@objc protocol FetchResponseExports: JSExport {
    var statusCode: Int { get }
    var body: String? { get }
    var headers: [String: String] { get }
}

@objc class FetchResponse: NSObject, FetchResponseExports {
    public var statusCode: Int = 0
    public var body: String?
    public var headers: [String: String] = [:]
    public var done: Bool = false
}

@objc protocol HMHomeJSExports: JSExport {
    var uuid: String { get }
    var name: String { get }
    var accessories: [String: HMAccJS] { get }
}

@objc protocol HMAccJSExports: JSExport {
    var uuid: String { get }
    var name: String { get }
    var roomUUID: String { get }
    var services: [String: HMSvcJS] { get }
}

@objc protocol HMSvcJSExports: JSExport {
    var uuid: String { get }
    var name: String { get }
    var accessory: HMAccJS { get }
    var characteristics: [String: HMCharJS] { get }
}

@objc protocol HMCharJSExports: JSExport {
    var valueLoaded: Bool { get }
    var displayName: String { get }
    var icon: String { get set }
    var characteristicType: String { get }
    var serviceUUID: String { get }
    var setValue: (@convention(block) (Any) -> JSValue)? { get }
    var getValue: (@convention(block) () -> JSValue)? { get }
}
