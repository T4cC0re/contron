//
//  Parameter.swift
//  Parameter
//
//  Created by Hendrik Meyer on 30.08.21.
//  Copyright © 2021 Hendrik Meyer. All rights reserved.
//

import Foundation
import JavaScriptCore

class Parameter: NSObject, IParameter, ObservableObject {
    var validValues: [NSNumber]? // TODO: Not implemented yet

    let name: String
    @Published var defaultValue: Any
    @Published var displayName: String
    @Published var icon: String = "slider.horizontal.3"
    @Published var minVal: Double?
    @Published var maxVal: Double?
    @Published var step: Double?
    @Published var value: Any {
        didSet {
            UserDefaults.standard.set(value, forKey: "user_script_parameter_\(name)")
        }
    }

    let isReadable: Bool
    let isWritable: Bool
    let isHidden: Bool
    let format: String?

    /**
     MUST NOT BE CALLED ON MAIN THREAD
     */
    init(name: String, defaultValue: Any, extra: JSValue?) {
        self.defaultValue = defaultValue
        displayName = name
        self.name = name
        value = defaultValue
        isReadable = true
        isWritable = true
        isHidden = false
        format = nil

        // Restore the value if it exists.
        // We do not however store any default values or ranges. Those come directly from the script.
        let existing = UserDefaults.standard.object(forKey: "user_script_parameter_\(name)")
        if existing == nil {
            value = defaultValue
        } else {
            value = existing!
        }

        guard let extra = extra, !extra.isNull, !extra.isUndefined, extra.isObject else {
            super.init()
            return
        }

        if !extra.objectForKeyedSubscript("displayName").isUndefined, !extra.objectForKeyedSubscript("displayName").isNull {
            let displayName = extra.objectForKeyedSubscript("displayName").toString()
            if displayName != nil, displayName != "" {
                self.displayName = displayName!
            }
        }

        if !extra.objectForKeyedSubscript("icon").isUndefined, !extra.objectForKeyedSubscript("icon").isNull {
            let icon = extra.objectForKeyedSubscript("icon").toString()
            if icon != nil, icon != "" {
                self.icon = icon!
            }
        }

        let minVal = extra.objectForKeyedSubscript("minVal").toNumber()
        if minVal != nil, !minVal!.doubleValue.isNaN {
            self.minVal = minVal!.doubleValue
        }

        let maxVal = extra.objectForKeyedSubscript("maxVal").toNumber()
        if maxVal != nil, !maxVal!.doubleValue.isNaN {
            self.maxVal = maxVal!.doubleValue
        }

        let step = extra.objectForKeyedSubscript("step").toNumber()
        if step != nil, !step!.doubleValue.isNaN {
            self.step = step!.doubleValue
        }

        super.init()
    }

    func resetToDefault() {
        Threading.uiThread.runSync {
            self.value = self.defaultValue
        }
    }
}
