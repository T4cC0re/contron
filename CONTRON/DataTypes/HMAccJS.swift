//
//  HMAccJS.swift
//  CONTRON
//
//  Created by Hendrik Meyer on 11.08.21.
//  Copyright © 2021 Hendrik Meyer. All rights reserved.
//

import Foundation
import HomeKit
import JavaScriptCore
import SwiftUI

@objc class HMAccJS: NSObject, HMAccJSExports, ObservableObject {
    @Published var uuid: String
    @Published var name: String
    @Published var roomUUID: String
    @Published var services: [String: HMSvcJS] = [:]
    @Published var accessory: HMAccessory
    public var isABridge: Bool {
        /*
         According to https://developer.apple.com/documentation/homekit/hmaccessory/1615278-uniqueidentifiersforbridgedacces
         an accessory is not a bridge, when `uniqueIdentifiersForBridgedAccessories` is nil.
         If `isBridged` is false, it can be either a bridge, or stand-alone.
         Thus, if `isBridged` is false, and `uniqueIdentifiersForBridgedAccessories` is NOT nil, this is a bridge.
         */
        return !accessory.isBridged && accessory.uniqueIdentifiersForBridgedAccessories != nil
    }

    public var isStandalone: Bool {
        // See `isABridge` for logic comments
        return !accessory.isBridged && accessory.uniqueIdentifiersForBridgedAccessories == nil
    }

    /**
     An identifier that we use to attach a HomeKitCodeEntry to an accessory.

     If available, we use the serial number as part of the identifier.
     If the returned string starts with `unreachable-` the accessory was unreachable and we did not know if there was a serial number.
         */
    public var homeKitCodeIdentifier: String {
        var identifierSalt = ""
        for svc in accessory.services {
            for char in svc.characteristics {
                if char.characteristicType == HMCharacteristicTypeSerialNumber {
                    identifierSalt = char.value as? String ?? ""
                }
            }
        }

        if identifierSalt == "" {
            if accessory.isReachable {
                identifierSalt = accessory.room?.name ?? "no_room"
            } else {
                // If the device is unreachable, we maybe did not get a serial number, even though it's there
                print("Device unreachable: " + uuid)
                identifierSalt = "unreachable"
            }
        }

        return (
            identifierSalt + "-" +
                (accessory.category.categoryType) + "-" +
                (accessory.manufacturer ?? "no_manufacturer") + "-" +
                (accessory.model ?? "no_model") + "-" +
                (isABridge ? "bridge" : "standalone")
        )
    }

    public static func from(accessory: HMAccessory) -> HMAccJS {
        var instance = HomeKitState.shared.getAccessory(uuid: accessory.uniqueIdentifier.uuidString)
        if instance == nil {
            instance = Threading.uiThread.runSync { HMAccJS(accessory: accessory) }
        }
        return instance!
    }

    public static func refreshOrCreate(accessory: HMAccessory, updateOnly: Bool) {
        guard let instance = HomeKitState.shared.getAccessory(uuid: accessory.uniqueIdentifier.uuidString) else {
            return Threading.uiThread.runSync { _ = HMAccJS(accessory: accessory) }
        }
        instance.refresh(updateOnly: updateOnly)
    }

    required init(accessory: HMAccessory) {
        accessory.delegate = HomeKitDelegate.shared

        name = accessory.name
        uuid = accessory.uniqueIdentifier.uuidString
        roomUUID = accessory.room?.uniqueIdentifier.uuidString ?? ""
        self.accessory = accessory

        super.init()

        HomeKitState.shared.setAccessory(accessory: self)
        loadServices(updateOnly: false)
    }

    private func loadServices(updateOnly: Bool) {
        if !updateOnly {
            services.removeAll()
        }

        for svc in accessory.services {
            var instance = HomeKitState.shared.getService(uuid: svc.uniqueIdentifier.uuidString)

            if instance != nil {
                instance!.refresh(updateOnly: updateOnly)
            } else {
                instance = Threading.uiThread.runSync { HMSvcJS(service: svc, accessory: self) }
            }

            Threading.uiThread.runSync { [self] in
                self.services[svc.uniqueIdentifier.uuidString] = instance
            }
        }

        // TODO: Remove entries not present in HomeKitManager
    }

    @available(*, deprecated, message: "use refresh(updateOnly: false)")
    public func forcefulRefresh() {
        refresh(updateOnly: false)
    }

    public func refresh(updateOnly: Bool) {
        Threading.uiThread.runSync {
            self.name = accessory.name
            self.uuid = accessory.uniqueIdentifier.uuidString
            self.roomUUID = accessory.room?.uniqueIdentifier.uuidString ?? ""
        }
        loadServices(updateOnly: updateOnly)
    }
}
