//
//  FriendlyNames.swift
//  CONTRON
//
//  Created by Hendrik Meyer on 11.04.21.
//  Copyright © 2021 Hendrik Meyer. All rights reserved.
//

import Foundation
import HomeKit

// var HomeKitCharacteristicFriendlyNames: Dictionary<String, String> = [:]
//    HMCharacteristicTypeActive: "Active",
//    HMCharacteristicTypeAdminOnlyAccess: "AdminOnlyAccess",
//    HMCharacteristicTypeAirParticulateDensity: "AirParticulateDensity",
//    HMCharacteristicTypeAirParticulateSize: "AirParticulateSize",
//    HMCharacteristicTypeAirQuality: "AirQuality",
//    HMCharacteristicTypeAudioFeedback: "AudioFeedback",
//    HMCharacteristicTypeBatteryLevel: "BatteryLevel",
//    HMCharacteristicTypeBrightness: "Brightness",
//    HMCharacteristicTypeCarbonDioxideDetected: "CarbonDioxideDetected",
//    HMCharacteristicTypeCarbonDioxideLevel: "CarbonDioxideLevel",
//    HMCharacteristicTypeCarbonDioxidePeakLevel: "CarbonDioxidePeakLevel",
//    HMCharacteristicTypeCarbonMonoxideDetected: "CarbonMonoxideDetected",
//    HMCharacteristicTypeCarbonMonoxideLevel: "CarbonMonoxideLevel",
//    HMCharacteristicTypeCarbonMonoxidePeakLevel: "CarbonMonoxidePeakLevel",
//    HMCharacteristicTypeChargingState: "ChargingState",
//    HMCharacteristicTypeColorTemperature: "ColorTemperature",
//    HMCharacteristicTypeContactState: "ContactState",
//    HMCharacteristicTypeCoolingThreshold: "CoolingThreshold",
//    HMCharacteristicTypeCurrentAirPurifierState: "CurrentAirPurifierState",
//    HMCharacteristicTypeCurrentDoorState: "CurrentDoorState",
//    HMCharacteristicTypeCurrentFanState: "CurrentFanState",
//    HMCharacteristicTypeCurrentHeaterCoolerState: "CurrentHeaterCoolerState",
//    HMCharacteristicTypeCurrentHeatingCooling: "CurrentHeatingCooling",
//    HMCharacteristicTypeCurrentHorizontalTilt: "CurrentHorizontalTilt",
//    HMCharacteristicTypeCurrentHumidifierDehumidifierState: "CurrentHumidifierDehumidifierState",
//    HMCharacteristicTypeCurrentLightLevel: "CurrentLightLevel",
//    HMCharacteristicTypeCurrentLockMechanismState: "CurrentLockMechanismState",
//    HMCharacteristicTypeCurrentPosition: "CurrentPosition",
//    HMCharacteristicTypeCurrentRelativeHumidity: "CurrentRelativeHumidity",
//    HMCharacteristicTypeCurrentSecuritySystemState: "CurrentSecuritySystemState",
//    HMCharacteristicTypeCurrentSlatState: "CurrentSlatState",
//    HMCharacteristicTypeCurrentTemperature: "CurrentTemperature",
//    HMCharacteristicTypeCurrentTilt: "CurrentTilt",
//    HMCharacteristicTypeCurrentVerticalTilt: "CurrentVerticalTilt",
//    HMCharacteristicTypeDehumidifierThreshold: "DehumidifierThreshold",
//    HMCharacteristicTypeDigitalZoom: "DigitalZoom",
//    HMCharacteristicTypeFilterChangeIndication: "FilterChangeIndication",
//    HMCharacteristicTypeFilterLifeLevel: "FilterLifeLevel",
//    HMCharacteristicTypeFilterResetChangeIndication: "FilterResetChangeIndication",
//    HMCharacteristicTypeHardwareVersion: "HardwareVersion",
//    HMCharacteristicTypeHeatingThreshold: "HeatingThreshold",
//    HMCharacteristicTypeHoldPosition: "HoldPosition",
//    HMCharacteristicTypeHue: "Hue",
//    HMCharacteristicTypeHumidifierThreshold: "HumidifierThreshold",
//    HMCharacteristicTypeIdentify: "Identify",
//    HMCharacteristicTypeImageMirroring: "ImageMirroring",
//    HMCharacteristicTypeImageRotation: "ImageRotation",
//    HMCharacteristicTypeInUse: "InUse",
//    HMCharacteristicTypeInputEvent: "InputEvent",
//    HMCharacteristicTypeIsConfigured: "IsConfigured",
//    HMCharacteristicTypeLabelIndex: "LabelIndex",
//    HMCharacteristicTypeLabelNamespace: "LabelNamespace",
//    HMCharacteristicTypeLeakDetected: "LeakDetected",
//    HMCharacteristicTypeLockManagementAutoSecureTimeout: "LockManagementAutoSecureTimeout",
//    HMCharacteristicTypeLockManagementControlPoint: "LockManagementControlPoint",
//    HMCharacteristicTypeLockMechanismLastKnownAction: "LockMechanismLastKnownAction",
//    HMCharacteristicTypeLockPhysicalControls: "LockPhysicalControls",
//    HMCharacteristicTypeLogs: "Logs",
//    HMCharacteristicTypeMotionDetected: "MotionDetected",
//    HMCharacteristicTypeMute: "Mute",
//    HMCharacteristicTypeName: "Name",
//    HMCharacteristicTypeNightVision: "NightVision",
//    HMCharacteristicTypeNitrogenDioxideDensity: "NitrogenDioxideDensity",
//    HMCharacteristicTypeObstructionDetected: "ObstructionDetected",
//    HMCharacteristicTypeOccupancyDetected: "OccupancyDetected",
//    HMCharacteristicTypeOpticalZoom: "OpticalZoom",
//    HMCharacteristicTypeOutletInUse: "OutletInUse",
//    HMCharacteristicTypeOutputState: "OutputState",
//    HMCharacteristicTypeOzoneDensity: "OzoneDensity",
//    HMCharacteristicTypePositionState: "PositionState",
//    HMCharacteristicTypePowerState: "PowerState",
//    HMCharacteristicTypeProgramMode: "ProgramMode",
//    HMCharacteristicTypeRemainingDuration: "RemainingDuration",
//    HMCharacteristicTypeRotationDirection: "RotationDirection",
//    HMCharacteristicTypeRotationSpeed: "RotationSpeed",
//    HMCharacteristicTypeSaturation: "Saturation",
//    HMCharacteristicTypeSecuritySystemAlarmType: "SecuritySystemAlarmType",
//    HMCharacteristicTypeSelectedStreamConfiguration: "SelectedStreamConfiguration",
//    HMCharacteristicTypeSerialNumber: "SerialNumber",
//    HMCharacteristicTypeSetDuration: "SetDuration",
//    HMCharacteristicTypeSetupStreamEndpoint: "SetupStreamEndpoint",
//    HMCharacteristicTypeSlatType: "SlatType",
//    HMCharacteristicTypeSmokeDetected: "SmokeDetected",
//    HMCharacteristicTypeSoftwareVersion: "SoftwareVersion",
//    HMCharacteristicTypeStatusActive: "StatusActive",
//    HMCharacteristicTypeStatusFault: "StatusFault",
//    HMCharacteristicTypeStatusJammed: "StatusJammed",
//    HMCharacteristicTypeStatusLowBattery: "StatusLowBattery",
//    HMCharacteristicTypeStatusTampered: "StatusTampered",
//    HMCharacteristicTypeStreamingStatus: "StreamingStatus",
//    HMCharacteristicTypeSulphurDioxideDensity: "SulphurDioxideDensity",
//    HMCharacteristicTypeSupportedAudioStreamConfiguration: "SupportedAudioStreamConfiguration",
//    HMCharacteristicTypeSupportedRTPConfiguration: "SupportedRTPConfiguration",
//    HMCharacteristicTypeSupportedVideoStreamConfiguration: "SupportedVideoStreamConfiguration",
//    HMCharacteristicTypeSwingMode: "SwingMode",
//    HMCharacteristicTypeTargetAirPurifierState: "TargetAirPurifierState",
//    HMCharacteristicTypeTargetDoorState: "TargetDoorState",
//    HMCharacteristicTypeTargetFanState: "TargetFanState",
//    HMCharacteristicTypeTargetHeaterCoolerState: "TargetHeaterCoolerState",
//    HMCharacteristicTypeTargetHeatingCooling: "TargetHeatingCooling",
//    HMCharacteristicTypeTargetHorizontalTilt: "TargetHorizontalTilt",
//    HMCharacteristicTypeTargetHumidifierDehumidifierState: "TargetHumidifierDehumidifierState",
//    HMCharacteristicTypeTargetLockMechanismState: "TargetLockMechanismState",
//    HMCharacteristicTypeTargetPosition: "TargetPosition",
//    HMCharacteristicTypeTargetRelativeHumidity: "TargetRelativeHumidity",
//    HMCharacteristicTypeTargetSecuritySystemState: "TargetSecuritySystemState",
//    HMCharacteristicTypeTargetTemperature: "TargetTemperature",
//    HMCharacteristicTypeTargetTilt: "TargetTilt",
//    HMCharacteristicTypeTargetVerticalTilt: "TargetVerticalTilt",
//    HMCharacteristicTypeTemperatureUnits: "TemperatureUnits",
//    HMCharacteristicTypeValveType: "ValveType",
//    HMCharacteristicTypeVersion: "Version",
//    HMCharacteristicTypeVolatileOrganicCompoundDensity: "VolatileOrganicCompoundDensity",
//    HMCharacteristicTypeVolume: "Volume",
//    HMCharacteristicTypeWaterLevel: "WaterLevel",
// ]

/**
 This was deprecated by Apple, but lots of manufacturers still use it.
 Because it is deprecated however, it is manually redefined here.
 */
let HMCharacteristicTypeSerialNumber = "00000030-0000-1000-8000-0026BB765291"

let HomeKitCharacteristicTypeManufacurerFriendlyNamesSem = DispatchSemaphore(value: 1)

var HomeKitCharacteristicTypeManufacurerFriendlyNames: [String: String] = [:]

func FriendlyNameForTypeKey(_ key: String, showUnknown: Bool) -> String? {
    HomeKitCharacteristicTypeManufacurerFriendlyNamesSem.wait()
//    guard let name = HomeKitCharacteristicFriendlyNames[key] ?? HomeKitCharacteristicTypeManufacurerFriendlyNames[key] else {
    guard let name = HomeKitCharacteristicTypeManufacurerFriendlyNames[key] else {
        HomeKitCharacteristicTypeManufacurerFriendlyNamesSem.signal()

        if showUnknown {
            return key
        }
        return nil
    }
    HomeKitCharacteristicTypeManufacurerFriendlyNamesSem.signal()

    return name
}

func RegisterFriendlyNameIfExists(_ characteristic: HMCharacteristic) {
//    if HomeKitCharacteristicFriendlyNames[characteristic.characteristicType] != nil {
//        return
//    }
    HomeKitCharacteristicTypeManufacurerFriendlyNamesSem.wait()

    guard HomeKitCharacteristicTypeManufacurerFriendlyNames[characteristic.characteristicType] != nil else {
        guard let manufacturerDescription = characteristic.metadata?.manufacturerDescription else {
            HomeKitCharacteristicTypeManufacurerFriendlyNamesSem.signal()
            return
        }

        HomeKitCharacteristicTypeManufacurerFriendlyNames[characteristic.characteristicType] = manufacturerDescription
        print("Custom Char: \(characteristic.characteristicType): \(manufacturerDescription)")
        HomeKitCharacteristicTypeManufacurerFriendlyNamesSem.signal()
        return
    }

    HomeKitCharacteristicTypeManufacurerFriendlyNamesSem.signal()
}
