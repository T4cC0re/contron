//
//  HMHomeJS.swift
//  HMHomeJS
//
//  Created by Hendrik Meyer on 31.08.21.
//  Copyright © 2021 Hendrik Meyer. All rights reserved.
//

import Foundation
import HomeKit
import JavaScriptCore
import SwiftUI

@objc class HMHomeJS: NSObject, HMHomeJSExports, ObservableObject {
    @Published var uuid: String
    @Published var name: String
    @Published var accessories: [String: HMAccJS] = [:]
    @Published var home: HMHome

    public static func from(home: HMHome) -> HMHomeJS {
        var instance = HomeKitState.shared.getHome(uuid: home.uniqueIdentifier.uuidString)
        if instance == nil {
            instance = HMHomeJS(home: home)
        }
        return instance!
    }

    public static func refreshOrCreate(home: HMHome, updateOnly: Bool) {
        guard let instance = HomeKitState.shared.getHome(uuid: home.uniqueIdentifier.uuidString) else {
            _ = HMHomeJS(home: home)
            return
        }
        instance.refresh(updateOnly: updateOnly)
    }

    required init(home: HMHome) {
        home.delegate = HomeKitDelegate.shared

        name = home.name
        uuid = home.uniqueIdentifier.uuidString
        self.home = home

        super.init()

        loadAccessories(updateOnly: false)

        HomeKitState.shared.setHome(home: self)
    }

    private func loadAccessories(updateOnly: Bool) {
        if !updateOnly {
            accessories.removeAll()
        }

        for acc in home.accessories {
            var instance = HomeKitState.shared.getAccessory(uuid: acc.uniqueIdentifier.uuidString)

            if instance != nil {
                instance!.refresh(updateOnly: updateOnly)
            } else {
                instance = Threading.uiThread.runSync { HMAccJS(accessory: acc) }
            }

            Threading.uiThread.runSync {
                self.accessories[acc.uniqueIdentifier.uuidString] = instance
            }
        }

        // TODO: Remove entries not present in HomeKitManager
    }

    @available(*, deprecated, message: "use refresh(updateOnly: false)")
    public func forcefulRefresh() {
        refresh(updateOnly: false)
    }

    public func refresh(updateOnly: Bool) {
        Threading.uiThread.runSync {
            self.name = self.home.name
            self.uuid = self.home.uniqueIdentifier.uuidString
        }
        loadAccessories(updateOnly: updateOnly)
    }
}
