//
//  HMSvcJS.swift
//  CONTRON
//
//  Created by Hendrik Meyer on 11.08.21.
//  Copyright © 2021 Hendrik Meyer. All rights reserved.
//

import Foundation
import HomeKit
import JavaScriptCore
import SwiftUI

@objc class HMSvcJS: NSObject, HMSvcJSExports, ObservableObject {
    @Published var uuid: String
    @Published var name: String
    @Published var characteristics: [String: HMCharJS] = [:]
    @Published var svc: HMService
    @Published var accessory: HMAccJS

    public static func from(service: HMService, accessory: HMAccJS) -> HMSvcJS {
        var instance = HomeKitState.shared.getService(uuid: service.uniqueIdentifier.uuidString)
        if instance == nil {
            instance = HMSvcJS(service: service, accessory: accessory)
        }
        return instance!
    }

    public static func refreshOrCreate(service: HMService, accessory: HMAccJS, updateOnly: Bool) {
        guard let instance = HomeKitState.shared.getService(uuid: service.uniqueIdentifier.uuidString) else {
            return Threading.uiThread.runSync { _ = HMSvcJS(service: service, accessory: accessory) }
        }
        instance.refresh(updateOnly: updateOnly)
    }

    /**
     Passing in a HMAccJS is required, as we cannot generate them without causing an infinite-loop.
     */
    required init(service: HMService, accessory: HMAccJS) {
        // no service delegates, so this is ignored for this type compared to HMAccJS and HMHomeJS
        name = service.name
        uuid = service.uniqueIdentifier.uuidString
        self.accessory = accessory
        svc = service

        super.init()

        HomeKitState.shared.setService(service: self)
        refresh(updateOnly: false)
        Threading.uiThread.runSync { [self] in
            self.accessory.services[uuid] = self
        }
    }

    @available(*, deprecated, message: "use refresh(updateOnly: Bool)")
    private func loadCharacteristics(updateOnly _: Bool) {
        //        var err: Error? = nil
    }

    @available(*, deprecated, message: "use refresh(updateOnly: false)")
    func forceRefresh() {
        refresh(updateOnly: false)
    }

    @available(*, deprecated, message: "use refresh(updateOnly: false)")
    public func forcefulRefresh() {
        refresh(updateOnly: false)
    }

    public func refresh(updateOnly: Bool) {
        Threading.uiThread.runSync {
            self.uuid = svc.uniqueIdentifier.uuidString
            self.name = svc.name
        }

        if !updateOnly {
            characteristics.removeAll()
        }

        Threading.uiThread.runSync {
            for char in self.svc.characteristics {
                // Self registering properties only work when this service is already registered.
                let instance = HMCharJS.getOrInit(characteristic: char)
                self.characteristics[instance.uuid] = instance
            }
        }

        for char in characteristics {
            char.value.refresh(updateOnly: updateOnly)
        }
    }

    public func addCharacteristic(_ char: HMCharJS) {
        Threading.uiThread.runSync {
            self.characteristics[char.uuid] = char
        }
    }

    typealias CompletionHandler = (_ success: Error?) -> Void

    func notify(title: String, subtitle: String) {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge]) { success, error in
            if success {
                let content = UNMutableNotificationContent()
                content.title = title
                content.subtitle = subtitle
                //                content.sound = UNNotificationSound.default

                let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)

                // choose a random identifier
                let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)

                // add our notification request
                UNUserNotificationCenter.current().add(request)

            } else if let error = error {
                print(error.localizedDescription)
            }
        }
    }

    @available(*, deprecated, message: "use localSetAndEnableNotifications on HMCharJS instance")
    private func localSetAndEnableNotifications(characteristic: HMCharacteristic, completionHandler: CompletionHandler?) {
        HMCharJS.getOrInit(characteristic: characteristic).localSetAndEnableNotifications(characteristic.value as Any) { _, _ in
            completionHandler?(nil)
        }
    }
}
