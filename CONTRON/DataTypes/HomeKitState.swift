//
//  HomeKitState.swift
//  CONTRON
//
//  Created by Hendrik Meyer on 14.08.21.
//  Copyright © 2021 Hendrik Meyer. All rights reserved.
//

import Foundation

@objc class HomeKitState: NSObject, ObservableObject {
    public static let shared = HomeKitState()
    @Published var accessories: [String: HMAccJS] = [:]
    @Published var services: [String: HMSvcJS] = [:]
    @Published var homes: [String: HMHomeJS] = [:]
    @Published var characteristics: [String: HMCharJS] = [:]
    @Published public var isAuthorized: Bool = false // Managed by HomeKitDelegate

    override private init() {
        super.init()
        _ = Timer(timeInterval: 300, target: self, selector: #selector(loadTimerFunc), userInfo: nil, repeats: true)
    }

    @objc private func loadTimerFunc() {
        if AppStage.shared.stage == .ready {
            HomeKitDelegate.shared.refreshHomes(updateOnly: true) {}
        }
    }

    @objc public func clearState() {
        Threading.uiThread.runSync {
            self.accessories = [:]
            self.homes = [:]
            self.services = [:]
            self.characteristics = [:]
        }
    }

    public func setAccessory(accessory: HMAccJS) {
        guard !accessory.uuid.isEmpty else {
            print("invalid accessory passed")
            return
        }

        Threading.uiThread.runSync {
            self.accessories[accessory.uuid] = accessory
        }
    }

    public func setService(service: HMSvcJS) {
        guard !service.uuid.isEmpty else {
            print("invalid service passed")
            return
        }

        Threading.uiThread.runSync {
            self.services[service.uuid] = service
        }
    }

    public func setHome(home: HMHomeJS) {
        guard !home.uuid.isEmpty else {
            print("invalid home passed")
            return
        }

        Threading.uiThread.runSync {
            self.homes[home.uuid] = home
        }
    }

    public func setCharacteristic(characteristic: HMCharJS) {
        guard !characteristic.uuid.isEmpty else {
            print("invalid characteristic passed")
            return
        }

        Threading.uiThread.runSync {
            self.characteristics[characteristic.uuid] = characteristic
            guard let instance = self.getService(uuid: characteristic.serviceUUID) else {
                return
            }
            instance.addCharacteristic(characteristic)
        }
    }

    public func getAccessory(uuid: String) -> HMAccJS? {
        return Threading.uiThread.runSync {
            return self.accessories[uuid]
        }
    }

    public func getService(uuid: String) -> HMSvcJS? {
        return Threading.uiThread.runSync {
            return self.services[uuid]
        }
    }

    public func getHome(uuid: String) -> HMHomeJS? {
        return Threading.uiThread.runSync {
            return self.homes[uuid]
        }
    }

    public func getCharacteristic(uuid: String) -> HMCharJS? {
        return Threading.uiThread.runSync {
            return self.characteristics[uuid]
        }
    }
}
