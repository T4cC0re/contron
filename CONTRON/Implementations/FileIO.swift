//
//  FileIO.swift
//  FileIO
//
//  Created by Hendrik Meyer on 08.09.21.
//  Copyright © 2021 Hendrik Meyer. All rights reserved.
//

import CryptoKit
import Foundation
import SwiftUI

// CryptoKit.Digest utils
extension Digest {
    var bytes: [UInt8] { Array(makeIterator()) }
    var data: Data { Data(bytes) }

    var hexStr: String {
        bytes.map { String(format: "%02X", $0) }.joined()
    }
}

enum StorageLocation {
    case undefined
    case local
    case iCloud
}

class FileIO: NSObject {
    public static let shared = FileIO()
    @AppStorage("save_in_icloud") var saveIniCloud: Bool = true

    override init() {
        super.init()
        if saveIniCloud, !isiCloudSupported {
            saveIniCloud = false
        }
    }

    /**
     Writes the content to disk or iCloud. The filename will be `SHA256(toHash) + fileExtension`. If `forceLocation` is not undefined, it will be obeyed. Otherwise iCloud is preferred.
     */
    func writeToFile(
        _ content: String,
        toHash: String,
        fileExtension: String = "",
        subDirectory: String? = nil,
        forceLocation: StorageLocation = .undefined
    ) -> StorageLocation {
        return writeToFile(
            content,
            toFile: SHA256.hash(data: toHash.data(using: .utf8)!).hexStr,
            fileExtension: fileExtension,
            subDirectory: subDirectory,
            forceLocation: forceLocation
        )
    }

    /**
     Writes the content to disk or iCloud. The filename will be `SHA256(toHash) + fileExtension`. If `forceLocation` is not undefined, it will be obeyed. Otherwise iCloud is preferred.
     */
    func writeToFile(
        _ content: Data,
        toHash: String,
        fileExtension: String = "",
        subDirectory: String? = nil,
        forceLocation: StorageLocation = .undefined
    ) -> StorageLocation {
        return writeToFile(
            content,
            toFile: SHA256.hash(data: toHash.data(using: .utf8)!).hexStr,
            fileExtension: fileExtension,
            subDirectory: subDirectory,
            forceLocation: forceLocation
        )
    }

    /**
     Writes the content to disk or iCloud. The filename will be `toFile + fileExtension`. If `forceLocation` is not undefined, it will be obeyed. Otherwise iCloud is preferred.
     */
    func writeToFile(
        _ content: String,
        toFile: String,
        fileExtension: String = "",
        subDirectory: String? = nil,
        forceLocation: StorageLocation = .undefined
    ) -> StorageLocation {
        return writeToFile(
            content.data(using: .utf8)!,
            toFile: toFile,
            fileExtension: fileExtension,
            subDirectory: subDirectory,
            forceLocation: forceLocation
        )
    }

    /**
     Writes the content to disk or iCloud. The filename will be `toFile + fileExtension`. If `forceLocation` is not undefined, it will be obeyed. Otherwise iCloud is preferred.
     */
    func writeToFile(
        _ content: Data,
        toFile: String,
        fileExtension: String = "",
        subDirectory: String? = nil,
        forceLocation: StorageLocation = .undefined
    ) -> StorageLocation {
        var location: StorageLocation
        var url: URL?
        switch forceLocation {
        case .undefined:
            if saveIniCloud {
                url = getiCloudDirectory(subDirectory: subDirectory)
                location = .iCloud
            } else {
                url = getLocalDirectory(subDirectory: subDirectory)
                location = .local
            }
        case .local:
            url = getLocalDirectory(subDirectory: subDirectory)
            location = .local
        case .iCloud:
            url = getiCloudDirectory(subDirectory: subDirectory)
            location = .iCloud
        }

        guard let url = url else {
            return .undefined
        }

        let fileURL = url.appendingPathComponent(toFile + fileExtension)

        do {
            try content.write(to: fileURL)
            let saved = FileManager.default.isReadableFile(atPath: fileURL.absoluteURL.path)
            print("saved: \(saved)")
        } catch {
            print(error)
            location = .undefined
        }

        return location
    }

    /**
     Read content from disk or iCloud. The filename will be `SHA256(toHash) + fileExtension`. If `forceLocation` is not undefined, it will be obeyed. Otherwise iCloud is preferred.
     */
    func readFromFile(
        fromHash: String,
        fileExtension: String = "",
        subDirectory: String? = nil,
        forceLocation: StorageLocation = .undefined
    ) -> (content: String?, location: StorageLocation) {
        return readFromFile(
            fromFile: SHA256.hash(data: fromHash.data(using: .utf8)!).hexStr,
            fileExtension: fileExtension,
            subDirectory: subDirectory,
            forceLocation: forceLocation
        )
    }

    /**
     Read content from disk or iCloud. The filename will be `SHA256(toHash) + fileExtension`. If `forceLocation` is not undefined, it will be obeyed. Otherwise iCloud is preferred.
     */
    func readFromFile(
        fromHash: String,
        fileExtension: String = "",
        subDirectory: String? = nil,
        forceLocation: StorageLocation = .undefined
    ) -> (content: Data?, location: StorageLocation) {
        return readFromFile(
            fromFile: SHA256.hash(data: fromHash.data(using: .utf8)!).hexStr,
            fileExtension: fileExtension,
            subDirectory: subDirectory,
            forceLocation: forceLocation
        )
    }

    /**
     Read content from disk or iCloud. The filename will be `toFile + fileExtension`. If `forceLocation` is not undefined, it will be obeyed. Otherwise iCloud is preferred.
     */
    func readFromFile(
        fromFile: String,
        fileExtension: String = "",
        subDirectory: String? = nil,
        forceLocation: StorageLocation = .undefined
    ) -> (content: String?, location: StorageLocation) {
        let data: (content: Data?, location: StorageLocation) = readFromFile(
            fromFile: fromFile,
            fileExtension: fileExtension,
            subDirectory: subDirectory,
            forceLocation: forceLocation
        )
        if data.content == nil {
            return (nil, .undefined)
        }
        return (String(data: data.content!, encoding: .utf8), data.location)
    }

    /**
     Read content from disk or iCloud. The filename will be `toFile + fileExtension`. If `forceLocation` is not undefined, it will be obeyed. Otherwise iCloud is preferred.
     */
    func readFromFile(
        fromFile: String,
        fileExtension: String = "",
        subDirectory: String? = nil,
        forceLocation: StorageLocation = .undefined
    ) -> (content: Data?, location: StorageLocation) {
        var url: URL?
        var location: StorageLocation = .undefined
        switch forceLocation {
        case .undefined:
            if saveIniCloud {
                url = getiCloudDirectory(subDirectory: subDirectory)
                location = .iCloud
            } else {
                url = getLocalDirectory(subDirectory: subDirectory)
                location = .local
            }
        case .local:
            url = getLocalDirectory(subDirectory: subDirectory)
            location = .local
        case .iCloud:
            url = getiCloudDirectory(subDirectory: subDirectory)
            location = .iCloud
        }

        guard let url = url else {
            return (nil, .undefined)
        }

        let fileURL = url.appendingPathComponent(fromFile + fileExtension)

        var data: Data?
        _ = fileURL.startAccessingSecurityScopedResource()
        do {
            data = try Data(contentsOf: fileURL)
        } catch {
            print(error)
        }
        fileURL.stopAccessingSecurityScopedResource()
        return (data, location)
    }

    func deleteByExtension(
        fileExtension: String,
        subDirectory: String? = nil,
        forceLocation: StorageLocation = .undefined
    ) {
        var url: URL?
        var location: StorageLocation = .undefined
        switch forceLocation {
        case .undefined:
            if saveIniCloud {
                url = getiCloudDirectory(subDirectory: subDirectory)
                location = .iCloud
            } else {
                url = getLocalDirectory(subDirectory: subDirectory)
                location = .local
            }
        case .local:
            url = getLocalDirectory(subDirectory: subDirectory)
            location = .local
        case .iCloud:
            url = getiCloudDirectory(subDirectory: subDirectory)
            location = .iCloud
        }

        guard let url = url else {
            return
        }

        let scripts = try? FileManager.default.contentsOfDirectory(atPath: url.path)
        guard scripts != nil, !scripts!.isEmpty else {
            return
        }
        for script in scripts! {
            if script.hasSuffix(fileExtension) {
                print("script: \(script)")
                _ = deleteFile(file: script, subDirectory: subDirectory, forceLocation: location)
            }
        }
    }

    func deleteFile(
        hash: String,
        fileExtension: String = "",
        subDirectory: String? = nil,
        forceLocation: StorageLocation = .undefined
    ) -> Bool {
        return deleteFile(file: SHA256.hash(data: hash.data(using: .utf8)!).hexStr, fileExtension: fileExtension, subDirectory: subDirectory, forceLocation: forceLocation)
    }

    func deleteFile(
        file: String,
        fileExtension: String = "",
        subDirectory: String? = nil,
        forceLocation: StorageLocation = .undefined
    ) -> Bool {
        var url: URL?
        switch forceLocation {
        case .undefined:
            if saveIniCloud {
                url = getiCloudDirectory(subDirectory: subDirectory)
            } else {
                url = getLocalDirectory(subDirectory: subDirectory)
            }
        case .local:
            url = getLocalDirectory(subDirectory: subDirectory)
        case .iCloud:
            url = getiCloudDirectory(subDirectory: subDirectory)
        }

        guard let url = url else {
            return false
        }

        let fileURL = url.appendingPathComponent(file + fileExtension)

        _ = fileURL.startAccessingSecurityScopedResource()
        do {
            try FileManager.default.removeItem(at: fileURL)
            fileURL.stopAccessingSecurityScopedResource()
            return true
        } catch {
            fileURL.stopAccessingSecurityScopedResource()
            return false
        }
    }

    /**
     This will return true if iCloud is enabled for the app in device settings.
     */
    public var isiCloudSupported: Bool {
        return getiCloudDirectory() != nil
    }

    // MARK: Support functions around paths

    /**
     Returns the app-scoped `Documents` directory in iCloud, or nil.
     */
    private func getiCloudDirectory(subDirectory: String? = nil) -> URL? {
        do {
            guard var url = FileManager.default.url(forUbiquityContainerIdentifier: nil)?.appendingPathComponent("Documents") else {
                return nil
            }
            if subDirectory != nil, subDirectory != "" {
                url = url.appendingPathComponent(subDirectory!)
            }
            try FileManager.default.createDirectory(at: url, withIntermediateDirectories: true, attributes: nil)
            return url
        } catch {
            return nil
        }
    }

    /**
     Returns the app-scoped `Documents` directory on the device, or nil.
     */
    private func getLocalDirectory(subDirectory: String? = nil) -> URL? {
        do {
            guard var url = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true) else {
                return nil
            }

            if subDirectory != nil, subDirectory != "" {
                url = url.appendingPathComponent(subDirectory!)
            }
            try FileManager.default.createDirectory(at: url, withIntermediateDirectories: true, attributes: nil)
            return url
        } catch {
            return nil
        }
    }
}
