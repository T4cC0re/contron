//
//  Bridge.swift
//  CONTRON
//
//  Created by Hendrik Meyer on 11.08.21.
//  Copyright © 2021 Hendrik Meyer. All rights reserved.
//

import Foundation
import SwiftUI

class CatalystBridge: NSObject, ICatalystCode {
    static let catalyst: CatalystBridge! = CatalystBridge()
    private static var _macSpecific: IMacOSSpecific?

    /**
            macSpecific returns IMacOSSpecific on macOS (or panics), returns nil on everything else (incl. iPad app on macOS)
     */
    static var macSpecific: IMacOSSpecific? {
        #if targetEnvironment(macCatalyst)
            if _macSpecific == nil {
                print("mac plugin init")
                let bundleFile = "macOSBridge.bundle"
                guard let bundleURL = Bundle.main.builtInPlugInsURL?.appendingPathComponent(bundleFile),
                      let bundle = Bundle(url: bundleURL),
                      let pluginClass = bundle.principalClass as? IMacOSSpecific.Type
                else {
                    fatalError("no mac exec")
                }
                _macSpecific = pluginClass.init()
                _macSpecific!.catalyst = CatalystBridge.catalyst // respond to mac events
                _macSpecific!.createBarItem()
            }
            return _macSpecific
        #else
            return nil
        #endif
    }

    func newWindow() {
        print("scene: newWindow")

        UIApplication.shared.requestSceneSessionActivation(SceneDelegate.activeScene?.session, userActivity: nil, options: nil) { e in
            print(e)
        }
    }

    func barItemClicked() {
        print("barItemClicked")
    }
}
