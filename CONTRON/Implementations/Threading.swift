//
//  Threading.swift
//  CONTRON
//
//  Created by Hendrik Meyer on 11.08.21.
//  Copyright © 2021 Hendrik Meyer. All rights reserved.
//

import Foundation

class Threading {
    private let queueKey = DispatchSpecificKey<Void>()
    private let queue: DispatchQueue
    private let operationQueue: OperationQueue
    private let mainThread: Bool

    private init(mainThread: Bool, label: String, QoS: DispatchQoS) {
        self.mainThread = mainThread
        operationQueue = OperationQueue()
        operationQueue.name = label
        operationQueue.maxConcurrentOperationCount = 16

        guard !mainThread else {
            queue = DispatchQueue.main
            operationQueue.underlyingQueue = queue
            return
        }

        queue = DispatchQueue(
            label: label,
            qos: QoS,
            attributes: .concurrent,
            autoreleaseFrequency: .workItem,
            target: DispatchQueue.global(qos: QoS.qosClass)
        )

        queue.setSpecific(key: queueKey, value: ())
    }

    public static let uiThread = Threading(mainThread: true, label: "UI/Main Thread", QoS: .userInteractive)

    public static let jsThread = Threading(mainThread: false, label: "JS Thread", QoS: .background)

    public static let homeKitThread = Threading(mainThread: false, label: "HomeKit Thread", QoS: .utility)

    public static let generalPurpose = Threading(mainThread: false, label: "general purpose Thread", QoS: .utility)

    public func runSync<T>(_ closure: () -> T) -> T {
        if (mainThread && Thread.isMainThread) || DispatchQueue.getSpecific(key: queueKey) != nil {
            // We are on the queue / main thread
            return closure()
        } else {
            return queue.sync(execute: closure)
        }
    }

    public func runAsync(_ closure: @escaping () -> Void) {
        if mainThread, Thread.isMainThread {
            print("WARN: runAsync called to run on a main thread. Running synchronously instead")
            closure()
        } else {
            queue.async(execute: closure)
        }
    }

    public func asyncAfter(deadline: DispatchTime, _ closure: @escaping () -> Void) {
        if mainThread, Thread.isMainThread {
            print("WARN: asyncAfter called to run on a main thread. Running synchronously immediately instead")
            closure()
        } else {
            queue.asyncAfter(deadline: deadline, execute: closure)
        }
    }

    public func enqueue(_ closure: @escaping () -> Void) {
        if mainThread, Thread.isMainThread {
            print("WARN: enqueue called to run on a main thread. Running synchronously instead")
            closure()
        } else {
            print("Threading: \(queue.label): Operations: \(operationQueue.operationCount + 1)/\(operationQueue.maxConcurrentOperationCount)")
            operationQueue.addOperation(closure)
        }
    }
}
