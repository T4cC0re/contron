//
//  Util.swift
//  Util
//
//  Created by Hendrik Meyer on 27.08.21.
//  Copyright © 2021 Hendrik Meyer. All rights reserved.
//

import Foundation
import UIKit

class Util: NSObject {
    static func presentAlert(alertController: UIAlertController, completion: (() -> Void)?) {
        Threading.uiThread.runSync {
            var rootViewController = UIApplication.shared.connectedScenes.compactMap { $0 as? UIWindowScene }.first?.windows.first?.rootViewController

            if let navigationController = rootViewController as? UINavigationController {
                rootViewController = navigationController.viewControllers.first
            }
            if let tabBarController = rootViewController as? UITabBarController {
                rootViewController = tabBarController.selectedViewController
            }
//            if rootViewController == nil {
//                preconditionFailure("no root view controller")
//            }

            rootViewController?.present(alertController, animated: true, completion: completion)
        }
    }

    static func crash(_ message: String) {
        // #if DEBUG
//            print("WARN: \(message)")
//            raise(SIGINT) // raise debugger
        // #else
        preconditionFailure(message)
        // #endif
    }
}
