//
//  JavaScriptBridge.swift
//  CONTRON
//
//  Created by Hendrik Meyer on 11.08.21.
//  Copyright © 2021 Hendrik Meyer. All rights reserved.
//

import BackgroundTasks
import HomeKit
import JavaScriptCore
import SwiftUI
import UIKit
import UserNotifications

enum JSBStage {
    case bootingOrReset
    case loadingBuiltin
    case builtInLoaded // Transitions into noScript or loadingScript eventually. This is set when nothing triggered a load yet.
    case noScript
    case loadingScript
    case loadError
    case userInit
    case userInitError
    case loaded
}

class JavaScriptBridge: NSObject, ObservableObject {
    private static var sharedInstance: JavaScriptBridge?
    public static var shared: JavaScriptBridge! {
        if sharedInstance == nil {
            sharedInstance = JavaScriptBridge()
        }
        return sharedInstance
    }

    private var vm: JSVirtualMachine?
    private var contextInstance: JSContext?
    public var context: JSContext? {
        contextInstance
    }

    @Published public var userScriptVersion = ""
    @Published public var parameters: [String: Parameter] = [:]
    @Published public var errors: [String: String] = [:]

    private var timer: Timer?

//    private var userScriptInitSemaphore = DispatchSemaphore(value: 1)
    private var currentlyLoadingScript: String?
    private var loadError: Error?
    private var userScriptObj: JSValue?
    @Published private(set) var stage: JSBStage
    public func setStage(_ target: JSBStage) {
        Threading.uiThread.runSync {
            self.stage = target
            if target == .loaded, self.timer == nil {
                self.timer = Timer.scheduledTimer(timeInterval: 30, target: self, selector: #selector(self.tickJS), userInfo: nil, repeats: true)
            }
        }
    }

    private func onLoadError(error: Error?, script: String, message: String) {
        var prefix = ""
        var title = ""
        switch true {
        case error as? ExtensionLoadError != nil:
            prefix = "Failed to load user script.\n"
            title = script
        case error as? NoExtensionScriptError != nil:
            title = script
            prefix = "User script does not have extension script header.\n"
        case error as? JSRuntimeError != nil:
            title = "JavaScript error"
            prefix = ""
        case error as? JSRuntimePanic != nil:
            title = "JavaScript PANIC"
            prefix = ""
        case error as? ExtensionInitError != nil:
            title = script
            prefix = "Failed to initialize user script.\n"
        default:
            title = "Unknown Error source"
            prefix = ""
        }

        let alertController = UIAlertController(title: title, message: prefix + message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))

        Threading.uiThread.runSync {
            self.errors[UUID().uuidString] = "[\(title)]\n\(prefix + message)"
        }

        Util.presentAlert(alertController: alertController, completion: nil)
        //        preconditionFailure("Failed to load User JS " + script + message)
    }

    public static func reset() {
        Threading.uiThread.runSync {
            if self.shared.stage == .loadingScript {
                Util.crash("Reset while script loading")
            }
            JavaScriptBridge.shared._init()
        }
    }

    override private init() {
        stage = .bootingOrReset
        super.init()
        Threading.uiThread.runSync {
            self._init()
        }
    }

    private func _init() {
        setStage(.bootingOrReset)
        vm = JSVirtualMachine()
        // TODO: This can crash if there is a userscript load in flight. -¬
//        self.userScriptInitSemaphore = DispatchSemaphore(value: 1) // <---'
        userScriptVersion = ""
        parameters = [:]
        errors = [:]
        print("JS: reset")

        // MARK: JavaScript

        contextInstance = JSContext(virtualMachine: vm!)
        contextInstance!.evaluateScript("var console = { log: function(message) { _consoleLog(message) } }")
        let consoleLog: @convention(block) (String) -> Void = { message in
            print("JS: " + message)
        }
        contextInstance!.setObject(unsafeBitCast(consoleLog, to: AnyObject.self), forKeyedSubscript: "_consoleLog" as (NSCopying & NSObjectProtocol)?)

        let loadScriptPromise: @convention(block) (String) -> JSValue = { url in
            JSValue(newPromiseIn: self.contextInstance, fromExecutor: { resolve, reject in
                Threading.jsThread.runAsync { [self] in
                    self.loadScript(url: url) { error in
                        guard error == nil else {
                            reject?.call(
                                withArguments: [
                                    JSValue(newErrorFromMessage: error?.localizedDescription, in: self.contextInstance)!,
                                ]
                            )
                            return
                        }
                        resolve?.call(withArguments: [])
                    }
                }
            })
        }

        let panic: @convention(block) (JSValue) -> Void = { message in
            self.onLoadError(error: JSRuntimePanic(), script: "JS Bridge", message: "JS: PANIC \(message)")
            //                preconditionFailure("JS: PANIC \(message)")
        }

        let loaded: @convention(block) (JSValue) -> Void = { version in
            print("JS:: loaded version \(version)")
            Threading.uiThread.runSync {
                self.userScriptVersion = "\(version)"
            }
        }

        contextInstance!.setObject(panic, forKeyedSubscript: "panic" as NSString)
        contextInstance!.setObject(loaded, forKeyedSubscript: "loaded" as NSString)
        contextInstance!.setObject(loadScriptPromise, forKeyedSubscript: "loadScript" as NSString)
        contextInstance!.setObject(findServices, forKeyedSubscript: "findServices" as NSString)
        contextInstance!.setObject(alert, forKeyedSubscript: "alert" as NSString)
        contextInstance!.setObject(findRoomByName, forKeyedSubscript: "findRoomByName" as NSString)
        contextInstance!.setObject(JSsleep, forKeyedSubscript: "sleep" as NSString)
        contextInstance!.setObject(request, forKeyedSubscript: "request" as NSString)
        contextInstance!.setObject(registerParameter, forKeyedSubscript: "registerParameter" as NSString)
        contextInstance!.setObject(getParameter, forKeyedSubscript: "getParameter" as NSString)
        contextInstance!.setObject(setParameter, forKeyedSubscript: "setParameter" as NSString)
        contextInstance!.setObject(setDisplayBrightness, forKeyedSubscript: "setDisplayBrightness" as NSString)

        contextInstance!.setObject(HMAccJS.self,
                                   forKeyedSubscript: "HMAccJS" as NSString)
        contextInstance!.setObject(HMSvcJS.self,
                                   forKeyedSubscript: "HMSvcJS" as NSString)
        userScriptObj = JSValue(newObjectIn: contextInstance)
        contextInstance!.setObject(userScriptObj!, forKeyedSubscript: "userscript" as NSString)

        contextInstance!.exceptionHandler = { _, exception in
            // type of String
            let stacktrace = exception?.objectForKeyedSubscript("stack").toString()
            // type of Number
            let lineNumber = exception?.objectForKeyedSubscript("line")
            // type of Number
            let column = exception?.objectForKeyedSubscript("column")
            let moreInfo = "in method \(String(describing: stacktrace))Line number in file: \(String(describing: lineNumber)), column: \(String(describing: column))"
            if self.currentlyLoadingScript != nil {
                self.onLoadError(error: self.loadError ?? ExtensionLoadError(), script: self.currentlyLoadingScript!, message: "\(String(describing: exception?.description)) \(moreInfo)")
            }
            print("Custom exception handler: \(exception?.description ?? "unknown error")")
            self.onLoadError(error: JSRuntimeError(), script: self.currentlyLoadingScript ?? "runtime", message: "\(String(describing: exception?.description)) \(moreInfo)")
        }

        Threading.jsThread.runSync {
            do {
                setStage(.loadingBuiltin)

                try loadScript(bundled: "bootstrap.js")
                try loadScript(bundled: "automations.js")

                setStage(.builtInLoaded)
            } catch {
                // These are bundled. If loading these fail, there is a problem.
                preconditionFailure(error.localizedDescription)
            }
        }
    }

    public var alert: (@convention(block) (String) -> Void)? {
        return { [] (message: String) in
            let alertController = UIAlertController(title: "JavaScript Alert", message: message, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Close", style: .cancel, handler: nil))

            Util.presentAlert(alertController: alertController, completion: nil)
        }
    }

    public func runUserScriptInit(_ closure: @escaping (_: Bool) -> Void) {
        guard stage == .builtInLoaded else {
            switch stage {
            case .loaded:
                closure(true)
            case .noScript, .loadError, .userInitError:
                closure(false)
            case .loadingScript, .userInit:
                Threading.jsThread.runAsync {
                    var fun: (() -> Void)?
                    // Wait until we have an end-state, then call the closure
                    let waitForEndState: () -> Void = { [fun] in
                        switch self.stage {
                        case .loaded:
                            closure(true)
                            return
                        case .loadError, .userInitError:
                            closure(false)
                            return
                        default:
                            // Sleep for 250 msec.
                            fun!()
                        }
                    }
                    fun = waitForEndState
                    Threading.jsThread.asyncAfter(deadline: .now() + 1, fun!)
                }
            case .bootingOrReset, .loadingBuiltin:
                Util.crash("WARN: runUserScriptInit Ignoring call in incorrect stage \(stage)")
            case .builtInLoaded:
                /*
                 Should never trigger, but switches must be exhaustive.
                 We don't want to add a default, so we fail the compile if we add another state.
                 This is done to prevent logic bugs.
                 */
                preconditionFailure("data race caused internal inconsistency.")
            }
            return
        }

        Threading.jsThread.runAsync { [self] in
            let url = UserDefaults.standard.string(forKey: "extension_url")
            guard let url = url, url != "" else {
                print("runUserScriptInit: no URL")
                setStage(.noScript)
                closure(false)
                return
            }
            // "https://gitlab.com/-/snippets/2106637/raw/master/main.js")

            setStage(.loadingScript)
            loadScript(url: url) { error in
                guard error == nil else {
                    print("runUserScriptInit: userScript failed \(error.debugDescription)")
                    setStage(.loadError)
                    self.onLoadError(error: self.loadError, script: "", message: "userScript failed")
                    closure(false)
                    return
                }

                print("runUserScriptInit: userScript eval'ed")
                self.runWithAwait(function: "load", withArguments: []) {
                    ret, err in
                    print("runUserScriptInit: extensionLoad promise")
                    if err != nil {
                        setStage(.loadError)
                        self.onLoadError(error: ExtensionLoadError(), script: url, message: err!.debugDescription)
                        self.currentlyLoadingScript = nil
                        closure(false)
                    } else {
                        guard let version = ret?.toString() else {
                            setStage(.loadError)
                            self.onLoadError(error: ExtensionLoadError(), script: url, message: "No version information returned.")
                            self.currentlyLoadingScript = nil
                            closure(false)
                            return
                        }
                        Threading.uiThread.runSync {
                            self.userScriptVersion = version
                        }
                        setStage(.userInit)
                        print("runUserScriptInit: version \(version)")
                        self.runWithAwait(function: "init", withArguments: []) {
                            _, err in
                            print("runUserScriptInit: extensionInit promise")
                            if err != nil {
                                setStage(.userInitError)
                                self.onLoadError(error: ExtensionInitError(), script: url, message: err!.debugDescription)
                                closure(false)
                                return
                            }
                            setStage(.loaded)
                            self.currentlyLoadingScript = nil
                            closure(true)
                        }
                    }
                }
            }
        }
    }

    /**
     Calls a function in JS and awaits the Promise. The closure called receives the return result or the error in the forst and second parameter, respectively.
     It's guaranteed that either the first of second JSValue esists, the other will be nil.
     */
    private func runWithAwait(function: String, withArguments: [Any]!, _ closure: @escaping (_: JSValue?, _: JSValue?) -> Void) {
        print("JS: runWithAwait: \(function)")
        guard let promise = userScriptObj?.invokeMethod(function, withArguments: withArguments), !promise.isNull,!promise.isUndefined else {
            print("JS: runWithAwait: no promise")
            closure(nil, JSValue(newErrorFromMessage: "could not get promise from \(function)", in: contextInstance))
            return
        }
        print("JS: runWithAwait: promise \(promise)")

        let out: @convention(block) (JSValue) -> Void = { out in
            closure(out, nil)
        }
        let outCb = JSValue(object: out, in: contextInstance)
        let err: @convention(block) (JSValue) -> Void = { err in
            self.onLoadError(error: JSRuntimeError(), script: "", message: err.debugDescription)

            closure(nil, err)
        }
        let errCb = JSValue(object: err, in: contextInstance)
        promise.invokeMethod("then", withArguments: [outCb as Any, errCb as Any])
    }

    @available(*, deprecated, message: "use loadScript(bundled:)")
    private func loadLocal(script: String) {
        try? loadScript(bundled: script)
    }

    private func loadScript(bundled: String) throws {
        currentlyLoadingScript = bundled
        let scriptURL = Bundle.main.url(
            forResource: "Scripts",
            withExtension: "bundle"
        )!.appendingPathComponent(bundled)

        guard FileManager.default.isReadableFile(atPath: scriptURL.path) else {
            preconditionFailure("bundled script \(bundled) unavailable")
        }

        do {
            try contextInstance?.evaluateScript(String(contentsOf: scriptURL), withSourceURL: scriptURL)
        } catch {
            currentlyLoadingScript = nil
            throw error
        }
        currentlyLoadingScript = nil
    }

    public func loadScript(url: String, callback: ((Error?) -> Void)?) {
        if stage != .loadingScript {
            Util.crash("loadScript called from unexpected stage \(stage)")
        }
        currentlyLoadingScript = url

        guard let url = URL(string: url), url.absoluteString != "" else {
            print("JS: no script... skipping...")
            callback?(nil)
            return
        }

        var script: UserScript?
        if url.scheme == "userscript" {
            let scriptname = url.absoluteString.replacingOccurrences(of: "userscript:", with: "")
            script = try? UserScript(scriptname, createIfNotExisting: false)
            // TODO: Might need createIfNotExisting==true
        } else {
            script = try? UserScript(libraryFromURL: url)
        }
        guard script != nil else {
            currentlyLoadingScript = nil
            callback?(ExtensionLoadError())
            return
        }
        contextInstance?.evaluateScript(script?.content, withSourceURL: url)

        currentlyLoadingScript = nil
        callback?(nil)
    }

    public func loadScript(script: UserScript, callback: ((Error?) -> Void)?) {
        contextInstance?.evaluateScript(script.content, withSourceURL: URL(string: "userscript:" + script.name)!)
        callback?(nil)
    }

    public var setDisplayBrightness: @convention(block) (JSValue) -> Void {
        return { [] (brightnessPercent: JSValue) in
            guard brightnessPercent.isNumber else {
                return
            }
            let brightness = brightnessPercent.toInt32()
            guard brightness <= 100, brightness > 1 else {
                return
            }
            let screenBrightness = Double(brightness) / 100
            Threading.uiThread.runSync {
                // Set the screen to thte desired brightness -1% first
                // This works around an issue, where a user side change
                // would bause the brightness to not be set when it is
                // changed to the same value we set it before.
                UIScreen.main.brightness = screenBrightness - 0.01
                UIScreen.main.brightness = screenBrightness
            }
        }
    }

    public var registerParameter: (@convention(block) (String, JSValue, JSValue?) -> JSValue)? {
        return { [] (name: String, defaultValue: JSValue, extra: JSValue?) in
            JSValue(newPromiseIn: self.contextInstance, fromExecutor: { resolve, reject in
                guard self.parameters[name] == nil else {
                    // Already registered. Skip it
                    Threading.jsThread.runAsync {
                        resolve?.call(withArguments: [])
                    }
                    return
                }
                guard let obj = defaultValue.toObject() else {
                    Threading.jsThread.runAsync {
                        reject?.call(withArguments: [JSValue(newErrorFromMessage: "no default value defined", in: self.contextInstance)!])
                    }
                    return
                }

                let parameter = Threading.jsThread.runSync {
                    Parameter(
                        name: name,
                        defaultValue: obj,
                        extra: extra
                    )
                }
                Threading.uiThread.runSync {
                    self.parameters[name] = parameter
                }

                print("parameter \(name) registered with \(defaultValue)")
                Threading.jsThread.runAsync {
                    resolve?.call(withArguments: [])
                }
            })
        }
    }

    public var getParameter: (@convention(block) (String) -> JSValue)? {
        return { [] (name: String) in
            JSValue(newPromiseIn: self.contextInstance, fromExecutor: { resolve, reject in
                print("parameter \(name) requested")
                guard let param = self.parameters[name] else {
                    Threading.jsThread.runAsync {
                        reject?.call(withArguments: [JSValue(newErrorFromMessage: "parameter is not registered", in: self.contextInstance)!])
                    }
                    return
                }

                Threading.jsThread.runAsync {
                    resolve?.call(withArguments: [JSValue(object: param.value, in: self.contextInstance)!])
                }
            })
        }
    }

    public var setParameter: (@convention(block) (String, JSValue) -> JSValue)? {
        return { [] (name: String, value: JSValue) in
            JSValue(newPromiseIn: self.contextInstance, fromExecutor: { resolve, reject in
                guard self.parameters[name] != nil else {
                    Threading.jsThread.runAsync {
                        resolve?.call(withArguments: [])
                    }
                    return
                }

                guard let obj = value.toObject() else {
                    Threading.jsThread.runAsync {
                        reject?.call(withArguments: [JSValue(newErrorFromMessage: "no value defined", in: self.contextInstance)!])
                    }
                    return
                }
                Threading.uiThread.runSync {
                    self.parameters[name]!.value = obj
                }
                print("parameter \(name) set to \(value)")

                Threading.jsThread.runAsync {
                    resolve?.call(withArguments: [])
                }
            })
        }
    }

    public var request: (@convention(block) (String, String, [String: String], String) -> JSValue)? {
        return { [] (method: String, url: String, _: [String: String], payload: String) in
            JSValue(newPromiseIn: self.contextInstance, fromExecutor: { resolve, reject in
                print("JS: Current thread in  \(#function) is \(Thread.current)")

                let send_url = URL(string: url)!
                var request = URLRequest(url: send_url)
                request.httpMethod = method
                request.cachePolicy = .reloadIgnoringLocalAndRemoteCacheData
                request.networkServiceType = .responsiveData
                request.allowsCellularAccess = true
                request.allowsExpensiveNetworkAccess = true
                request.allowsConstrainedNetworkAccess = true

                var send_payload: Data?
                if method != "GET" {}
                let fresponse = FetchResponse()

                var task: URLSessionTask
                if method != "GET" {
                    send_payload = payload.data(using: .utf8)

                    task = URLSession.shared.uploadTask(with: request, from: send_payload) { data, response, error in
                        if let error = error {
                            print("error: \(error)")
                            reject?.call(withArguments: [error.localizedDescription.debugDescription])
                            return
                        }

                        fresponse.body = String(data: data!, encoding: .utf8)
                        //            fresponse.headers = (response as! HTTPURLResponse).allHeaderFields
                        fresponse.statusCode = (response as! HTTPURLResponse).statusCode
                        resolve?.call(withArguments: [fresponse])
                    }

                } else {
                    task = URLSession.shared.downloadTask(with: request) { localURL, response, error in
                        if let error = error {
                            print("error: \(error)")
                            reject?.call(withArguments: [error.localizedDescription.debugDescription])
                            return
                        }

                        if let localURL = localURL {
                            if let string = try? String(contentsOf: localURL) {
                                fresponse.body = string
                            }
                        }
                        //            fresponse.headers = (response as! HTTPURLResponse).allHeaderFields
                        fresponse.statusCode = (response as! HTTPURLResponse).statusCode
                        resolve?.call(withArguments: [fresponse])
                    }
                }
                task.resume()
            })
        }
    }

    @objc public func tickJS() {
        if stage == .loaded {
            runWithAwait(function: "tick", withArguments: []) { _, _ in }
        } else {
            print("WARN: tickJS called when not loaded. \(stage)")
        }
    }

    /**
     sleep X ms
     */
    public var JSsleep: (@convention(block) (Int) -> Void)? {
        return { [] (duration_ms: Int) in
            let group = DispatchGroup()
            group.enter()
            Threading.jsThread.runAsync {
                sleep(UInt32(duration_ms / 1000))
                group.leave()
            }
            group.wait()
        }
    }

    public var findServices: (@convention(block) (String, String, String) -> JSValue)? {
        return { [] (name: String, roomUUID: String, withCharacteristic: String) in
            JSValue(newPromiseIn: self.contextInstance, fromExecutor: { resolve, _ in

                var retVal: [HMSvcJS] = []
                let intID: String? = (roomUUID == "undefined" || roomUUID == "null" || roomUUID == "") ? nil : roomUUID
                let intName: String? = (name == "undefined" || name == "null" || name == "") ? nil : name
                let intChar: String? = (withCharacteristic == "undefined" || withCharacteristic == "null" || withCharacteristic == "") ? nil : withCharacteristic
                //        print("findServices: name",intName as Any, "roomUUID", intID as Any, "withCharacteristic", intChar as Any)

                for service in HomeKitState.shared.services {
                    if intName == nil || service.value.accessory.name == intName || service.value.name == intName {
                        if intID == nil || service.value.accessory.roomUUID == intID {
                            if intChar == nil || service.value.characteristics.index(forKey: intChar!) != nil {
                                retVal.append(service.value)
                            }
                        }
                    }
                }

                resolve?.call(withArguments: [retVal])

            })
        }
    }

    public var findRoomByName: (@convention(block) (String) -> JSValue)? {
        return { [] (name: String) in
            JSValue(newPromiseIn: self.contextInstance, fromExecutor: { resolve, _ in

                for home in HomeKitDelegate.shared.mgr.homes {
                    for room in home.rooms {
                        if room.name == name {
                            resolve?.call(withArguments: [room.uniqueIdentifier.uuidString])
                        }
                    }
                }

                resolve?.call(withArguments: [JSValue(nullIn: self.contextInstance)!])
            })
        }
    }

    public func pushUpdateToJS(service: HMSvcJS, characteristic: HMCharacteristic) {
        //    print("Update Characteristic", service.mapCharacteristicName(characteristic: characteristic), "on", service.name, characteristic.value)

        contextInstance?.objectForKeyedSubscript("broadcastUpdateCharacteristic").call(withArguments: [service, FriendlyNameForTypeKey(characteristic.characteristicType, showUnknown: true)!])
    }
}
