//
//  AppStage.swift
//  AppStage
//
//  Created by Hendrik Meyer on 01.09.21.
//  Copyright © 2021 Hendrik Meyer. All rights reserved.
//

import Foundation

enum AppStageEnum {
    case booting
    case beforeAuthorization
    case afterAuthorization
    case stateReset
    case homeKitLoading
    case userScriptLoading
    case ready
}

class AppStage: NSObject, ObservableObject {
    static let shared = AppStage()
    /**
     Do not set this manually. Use AppStage.set()
     */
    @Published var stage: AppStageEnum = .booting {
        didSet {
            Threading.uiThread.runSync {
                switch self.stage {
                case .beforeAuthorization:
                    HomeKitState.shared.isAuthorized = false
                    Threading.homeKitThread.runAsync {
                        _ = HomeKitDelegate.shared.probeAuthorization()
                    }
                case .afterAuthorization:
                    HomeKitState.shared.isAuthorized = true
                    self.stage = .stateReset
                    return
                case .stateReset:
                    JavaScriptBridge.reset()
                    HomeKitState.shared.clearState()
                    self.stage = .homeKitLoading
                case .homeKitLoading:
                    HomeKitDelegate.shared.refreshHomes(updateOnly: true) {
                        Threading.uiThread.runSync {
                            self.stage = .userScriptLoading
                        }
                    }
                    return
                case .userScriptLoading:
                    JavaScriptBridge.shared.runUserScriptInit { _ in
                        Threading.uiThread.runSync {
                            self.stage = .ready
                        }
                    }
                    fallthrough
                case .ready:
                    fallthrough
                default:
                    // Nothing to do.
                    return
                }
            }
        }
    }

    public static func set(_ stage: AppStageEnum) {
        Threading.uiThread.runSync {
            if stage == .userScriptLoading, AppStage.shared.stage != .ready {
                print("WARN: Tried to set userScriptLoading from a non-ready state")
                return
            }
            AppStage.shared.stage = stage
        }
    }
}
