//
//  HomeKitDelegate.swift
//  CONTRON
//
//  Created by Hendrik Meyer on 24.04.21.
//  Copyright © 2021 Hendrik Meyer. All rights reserved.
//

import BackgroundTasks
import Foundation
import HomeKit
import JavaScriptCore
import SwiftUI

class HomeKitDelegate: NSObject, HMAccessoryDelegate, HMHomeDelegate, HMHomeManagerDelegate, ObservableObject {
    let mgr = HMHomeManager()
    public static let shared = HomeKitDelegate()
    var timer: Timer?
    var authTimer: Timer?
    private var calls = 0

    override private init() {
        super.init()
        mgr.delegate = self
    }

    /**
     To be called when the app goes in foreground.
     This is a convenience function to signify the purpose to external callers and handle threading
     */
    @available(*, deprecated, message: "Transition the app into state .beforeAuthorization instead")
    func startWaitingForAuthorization() {
        Threading.homeKitThread.runAsync {
            _ = self.probeAuthorization()
        }
    }

    /**
     To be called when the app goes in background or when authorization was granted.
     */
    func stopWaitingForAuthorization() {
        authTimer?.invalidate()
        authTimer = nil
    }

    /**
     Will look if authorization is given.
     If not, it will start a timer to keep checking for it.
     If yes, it will stop any timers looking for authorization.
     If the state transitions from unauthorized to authorized, the JavaScriptBridge.runJSinit is called, followed by startLookingForAccessories.
     */
    @objc func probeAuthorization() -> Bool {
        if mgr.authorizationStatus.contains(.authorized) {
            // Cancel the timer
            stopWaitingForAuthorization()

            // We are authorized. Call Closure
            if HomeKitState.shared.isAuthorized {
                // Already authorized. Nothing to do
                return HomeKitState.shared.isAuthorized
            }

            AppStage.set(.afterAuthorization)
        } else {
            if authTimer == nil {
                authTimer = Timer.scheduledTimer(timeInterval: 0.25, target: self, selector: #selector(probeAuthorization), userInfo: nil, repeats: true)
            }

            AppStage.set(.beforeAuthorization)
        }
        return HomeKitState.shared.isAuthorized
    }

    // MARK: Delegates

    func home(_ home: HMHome, didAdd _: HMAccessory) {
        HMHomeJS.refreshOrCreate(home: home, updateOnly: true)
    }

    func home(_: HMHome, didRemove _: HMAccessory) {
        // TODO: implement properly
        //        refreshAndBroadcast(home: home)
    }

    func home(_ home: HMHome,
              didUpdate _: HMHomeHubState)
    {
        HMHomeJS.refreshOrCreate(home: home, updateOnly: true)
    }

    func accessoryDidUpdateName(_ accessory: HMAccessory) {
        HMAccJS.refreshOrCreate(accessory: accessory, updateOnly: true)
    }

    func accessory(_: HMAccessory,
                   service _: HMService,
                   didUpdateValueFor characteristic: HMCharacteristic)
    {
        HMCharJS.getOrInit(characteristic: characteristic).refresh(updateOnly: true)
    }

    func accessory(_ accessory: HMAccessory,
                   didUpdateNameFor _: HMService)
    {
        print("didUpdateNameForService")
        HMAccJS.refreshOrCreate(accessory: accessory, updateOnly: true)
    }

    func home(_ home: HMHome, didUpdateNameFor _: HMRoom) {
        print("didUpdateNameForRoom")
        HMHomeJS.refreshOrCreate(home: home, updateOnly: true)
    }

    func home(_ home: HMHome, didAdd _: HMServiceGroup) {
        print("didAdd group: HMServiceGroup")
        HMHomeJS.refreshOrCreate(home: home, updateOnly: true)
    }

    func home(_ home: HMHome, didRemove _: HMServiceGroup) {
        print("didRemove group: HMServiceGroup")
        HMHomeJS.refreshOrCreate(home: home, updateOnly: false)
    }

    func homeDidUpdateSupportedFeatures(_ home: HMHome) {
        print("homeDidUpdateSupportedFeatures")
        HMHomeJS.refreshOrCreate(home: home, updateOnly: true)
    }

    func home(_ home: HMHome, didUpdateNameFor _: HMServiceGroup) {
        print("didUpdateNameFor group: HMServiceGroup")
        HMHomeJS.refreshOrCreate(home: home, updateOnly: true)
    }

    func home(_ home: HMHome, didUnblockAccessory _: HMAccessory) {
        print("didUnblockAccessory")
        HMHomeJS.refreshOrCreate(home: home, updateOnly: true)
    }

    func home(_ home: HMHome, didUpdate _: HMRoom, for _: HMAccessory) {
        print("didUpdate room: HMRoom, for accessory: HMAccessory")
        HMHomeJS.refreshOrCreate(home: home, updateOnly: true)
    }

    func home(_ home: HMHome, didRemove _: HMRoom) {
        print("didRemove room: HMRoom")
        HMHomeJS.refreshOrCreate(home: home, updateOnly: false)
    }

    func home(_ home: HMHome, didAdd _: HMService, to _: HMServiceGroup) {
        print("didAdd service: HMService, to group: HMServiceGroup")
        HMHomeJS.refreshOrCreate(home: home, updateOnly: true)
    }

    func home(_ home: HMHome, didRemove _: HMService, from _: HMServiceGroup) {
        print("didRemove service: HMService, from group: HMServiceGroup")
        HMHomeJS.refreshOrCreate(home: home, updateOnly: false)
    }

    func home(_: HMHome, didEncounterError error: Error, for accessory: HMAccessory) {
        print("didEncounterError", error)
        HMAccJS.refreshOrCreate(accessory: accessory, updateOnly: true)
    }

    func accessory(_ accessory: HMAccessory, didAdd _: HMAccessoryProfile) {
        print("HMAccessory, didAdd profile")
        HMAccJS.refreshOrCreate(accessory: accessory, updateOnly: true)
    }

    func accessory(_ accessory: HMAccessory, didRemove _: HMAccessoryProfile) {
        print("HMAccessory, didRemove profile")
        HMAccJS.refreshOrCreate(accessory: accessory, updateOnly: false)
    }

    func accessory(_ accessory: HMAccessory, didUpdateFirmwareVersion _: String) {
        print("HMAccessory, didUpdateFirmwareVersion")
        HMAccJS.refreshOrCreate(accessory: accessory, updateOnly: true)
    }

    func homeManagerDidUpdateHomes(_: HMHomeManager) {
        print("homeManagerDidUpdateHomes")
        AppStage.set(.homeKitLoading)
    }

    func homeManager(_: HMHomeManager, didAdd home: HMHome) {
        print("HMHomeManager, didAdd home")
        HMHomeJS.refreshOrCreate(home: home, updateOnly: true)
    }

    func homeManager(_: HMHomeManager, didRemove home: HMHome) {
        print("HMHomeManager, didRemove home")
        HMHomeJS.refreshOrCreate(home: home, updateOnly: false)
    }

    func homeManager(_: HMHomeManager, didUpdate _: HMHomeManagerAuthorizationStatus) {
        print("HMHomeManager, didUpdate status")
        AppStage.set(.homeKitLoading)
    }

    #if !targetEnvironment(macCatalyst)
        func homeManager(_: HMHomeManager, didReceiveAddAccessoryRequest _: HMAddAccessoryRequest) {
            print("HMHomeManager, didReceiveAddAccessoryRequest")
            AppStage.set(.homeKitLoading)
        }
    #endif

    @available(*, deprecated)
    func refreshAndBroadcast(service: HMService) {
        //        queue.async { [self] in
        service.accessory?.delegate = self
        HomeKitState.shared.getService(uuid: service.uniqueIdentifier.uuidString)?.forceRefresh()
        //        }
    }

    @available(*, deprecated, message: "use HMAccJS.refreshOrCreate(accessory:, updateOnly:)")
    func refreshAndBroadcast(accessory: HMAccessory) {
        //        queue.async { [self] in
        accessory.delegate = self
        HMAccJS.refreshOrCreate(accessory: accessory, updateOnly: true)
        HomeKitState.shared.getAccessory(uuid: accessory.uniqueIdentifier.uuidString)?.forcefulRefresh()
        //        }
    }

    func accessory(_ accessory: HMAccessory, didUpdateAssociatedServiceTypeFor _: HMService) {
        HMAccJS.refreshOrCreate(accessory: accessory, updateOnly: true)
    }

    func accessoryDidUpdateServices(_ accessory: HMAccessory) {
        HMAccJS.refreshOrCreate(accessory: accessory, updateOnly: true)
    }

    func accessoryDidUpdateReachability(_ accessory: HMAccessory) {
        HMAccJS.refreshOrCreate(accessory: accessory, updateOnly: true)
    }

    var sem = DispatchSemaphore(value: 1)

    func refreshHomes(updateOnly: Bool, _ closure: @escaping () -> Void) {
        Threading.homeKitThread.runAsync {
            for home in self.mgr.homes {
                HMHomeJS.refreshOrCreate(home: home, updateOnly: updateOnly)
            }
            closure()
        }
    }
}
