//
//  SceneDelegate.swift
//  CONTRON
//
//  Created by Hendrik Meyer on 25.02.20.
//  Copyright © 2020 Hendrik Meyer. All rights reserved.
//

import SwiftUI
import UIKit

class SceneDelegate: UIResponder, UISceneDelegate, UIWindowSceneDelegate {
    var window: UIWindow?
    static var firstClosed: Bool = false
    static var activeScene: UIScene?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options _:
        UIScene.ConnectionOptions)
    {
        print("scene: willConnectTo")
        guard let windowScene = (scene as? UIWindowScene) else { return }

        #if targetEnvironment(macCatalyst)
            if !SceneDelegate.firstClosed && UserDefaults.standard.bool(forKey: "start_minimized") {
                UIApplication.shared.requestSceneSessionDestruction(session, options: .none) { e in
                    print(e)
                }
                SceneDelegate.firstClosed = true
                return
            }
            if let titlebar = windowScene.titlebar {
                titlebar.titleVisibility = .visible
                titlebar.separatorStyle = .none
            }
        #endif

        let window = UIWindow(windowScene: windowScene)
        window.rootViewController = UIHostingController(rootView: view) // Your initial view controller.
        window.makeKeyAndVisible()
        self.window = window
        SceneDelegate.activeScene = scene
    }

    func sceneDidDisconnect(_: UIScene) {
        print("scene: sceneDidDisconnect")
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).

        // TODO: Send a notification stating, that the app is still running in the background
        SceneDelegate.activeScene = nil
    }

    func sceneDidBecomeActive(_: UIScene) {
        print("scene: sceneDidBecomeActive")
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
        CatalystBridge.macSpecific?.disableMaximizeButton()
//        HomeKitDelegate.shared.startWaitingForAuthorization()
    }

    func sceneWillResignActive(_: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
        print("scene: sceneWillResignActive")
        HomeKitDelegate.shared.stopWaitingForAuthorization()
    }

    func sceneWillEnterForeground(_: UIScene) {
        print("scene: sceneWillEnterForeground")
    }

    func windowScene(_: UIWindowScene, didUpdate _: UICoordinateSpace, interfaceOrientation _: UIInterfaceOrientation, traitCollection _: UITraitCollection) {
        print("scene: didUpdate")
        CatalystBridge.macSpecific?.disableMaximizeButton()
    }

    func sceneDidEnterBackground(_: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
        print("scene: sceneDidEnterBackground")
    }
}
