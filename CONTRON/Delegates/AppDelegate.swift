//
//  AppDelegate.swift
//  CONTRON
//
//  Created by Hendrik Meyer on 25.02.20.
//  Copyright © 2020 Hendrik Meyer. All rights reserved.
//

import SwiftUI

struct NoExtensionScriptError: Error {}
struct ExtensionLoadError: Error {}
struct ExtensionInitError: Error {}
struct JSRuntimeError: Error {}
struct JSRuntimePanic: Error {}
class DownloadError: Error {
    var statusCode: Int
    init(_ statusCode: Int) {
        self.statusCode = statusCode
    }
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    override init() {
        // MARK: Prepare settings

        // This will load the plist and make sync default values to the system settings
        let settingsUrl = Bundle.main.url(
            forResource: "Settings",
            withExtension: "bundle"
        )!.appendingPathComponent("Root.plist")
        let settingsPlist = NSDictionary(contentsOf: settingsUrl)!
        let preferences = settingsPlist["PreferenceSpecifiers"] as! [NSDictionary]

        var defaultsToRegister = [String: Any]()

        for preference in preferences {
            guard let key = preference["Key"] as? String else {
                continue
            }
            defaultsToRegister[key] = preference["DefaultValue"]
        }

        UserDefaults.standard.register(defaults: defaultsToRegister)
        UserDefaults.standard.synchronize()

        super.init()
    }

    override func buildMenu(with builder: UIMenuBuilder) {
        print("AppDelegate: buildMenu")
        super.buildMenu(with: builder)

        // https://zachsim.one/blog/2019/8/4/customising-the-menu-bar-of-a-catalyst-app-using-uimenubuilder
        builder.remove(menu: .services)
        builder.remove(menu: .format)
        builder.remove(menu: .toolbar)
        builder.remove(menu: .file)
        builder.remove(menu: .edit)
        builder.remove(menu: .view)
        builder.remove(menu: .window)
        builder.remove(menu: .help)
    }

    func application(_: UIApplication, willFinishLaunchingWithOptions _: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        print("AppDelegate: willFinishLaunchingWithOptions")

        return true
    }

    func applicationSignificantTimeChange(_: UIApplication) {
        print("AppDelegate: applicationSignificantTimeChange")
        HomeKitDelegate.shared.refreshHomes(updateOnly: true) {}
    }

    func applicationDidFinishLaunching(_: UIApplication) {
        print("AppDelegate: applicationDidFinishLaunching")

        AppStage.set(.beforeAuthorization)
    }

    func application(_: UIApplication, didUpdate _: NSUserActivity) {
        print("AppDelegate: didUpdate")
    }
}
