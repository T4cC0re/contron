//
//  UserScriptView.swift
//  UserScriptView
//
//  Created by Hendrik Meyer on 31.08.21.
//  Copyright © 2021 Hendrik Meyer. All rights reserved.
//

import SwiftUI

struct UserScriptView: View {
    @ObservedObject var userScript: UserScript
    @AppStorage("extension_url") var extensionURL: String = ""

    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                Button(action: {
                    do {
                        try userScript.save()
                        extensionURL = "userscript:\(userScript.name)"
                    } catch {
                        let alertController = UIAlertController(title: "Error while saving", message: error.localizedDescription, preferredStyle: .alert)
                        alertController.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
                        Util.presentAlert(alertController: alertController, completion: nil)
                    }
                }, label: {
                    Label("Save", systemImage: "tray.and.arrow.down")
                }).disabled(userScript.editState == .saved)
                Divider().padding(.leading)
                switch userScript.editState {
                case .saved:
                    Label("Saved", systemImage: "checkmark.circle")
                case .unsaved:
                    Label("Unsaved changes", systemImage: "pencil.circle")
                case .error:
                    Label("Unable to save", systemImage: "exclamationmark.circle")
                }
                Spacer()
                RestartJSButton().disabled(userScript.editState != .saved)
            }
            .fixedSize(horizontal: false, vertical: true).padding(.all, 16)

            TextEditor(text: $userScript.content)
//                .font(.custom("Source Code Pro", size: 16))
                //                            .font(.system(.body, design: .monospaced))
                .disableAutocorrection(true)
                .keyboardType(.default)
        }.navigationBarTitleDisplayMode(.inline).navigationTitle("User Script Code")
    }
}

struct UserScriptView_Previews: PreviewProvider {
    static var previews: some View {
        Text("mocl")
    }
}
