//
//  JavaScriptBridgeView.swift
//  JavaScriptBridgeView
//
//  Created by Hendrik Meyer on 29.08.21.
//  Copyright © 2021 Hendrik Meyer. All rights reserved.
//

import SwiftUI

struct RestartJSButton: View {
    @ObservedObject var appStage = AppStage.shared

    var body: some View {
        Button(action: {
            AppStage.set(.stateReset)
        }) {
            Label("Clear State & reload", systemImage: "arrow.counterclockwise")
        }
    }
}

struct JavaScriptBridgeView: View {
    @ObservedObject var javascriptBridge: JavaScriptBridge
    @State private var collapsed: Bool = true
    @ObservedObject private var userScript: UserScript = try! UserScript("main", createIfNotExisting: true)
    @State private var isShowingScriptEditor = false
    @AppStorage("extension_url") var extensionURL: String = ""

    var body: some View {
        Form {
            Section(header: Text("User Script")) {
                HStack {
                    Label("Script source", systemImage: "link")
                    Spacer()
                    Text(extensionURL)
                }

                HStack {
                    Label("Script Location", systemImage: "link")
                    Spacer()
                    if userScript.location == .undefined {
                        Label("Not saved", systemImage: "xmark")
                    } else if userScript.location == .iCloud {
                        Label("iCloud", systemImage: "icloud.fill")
                    } else if userScript.location == .local {
                        Label("on device", systemImage: "internaldrive.fill")
                    } else {
                        Label("¯\\_(ツ)_/¯", systemImage: "questionmark.triangle")
                    }
                }

                NavigationLink(
                    destination: UserScriptView(userScript: userScript), isActive: $isShowingScriptEditor
                ) {
                    if userScript.isPlaceholder {
                        Label("Create User Script", systemImage: "plus")
                    } else {
                        Label("Edit User Script", systemImage: "chevron.left.slash.chevron.right")
                    }
                }.disabled(javascriptBridge.stage != .loaded && javascriptBridge.stage != .noScript && javascriptBridge.stage != .loadError)

                HStack {
                    if javascriptBridge.stage == .loaded {
                        Label("Version", systemImage: "number")
                        Spacer()
                        Text(javascriptBridge.userScriptVersion)
                    } else if javascriptBridge.stage == .loadError {
                        Label("Error while loading", systemImage: "exclamationmark.triangle")
                    } else if javascriptBridge.stage == .userInitError {
                        Label("Error while initializing", systemImage: "exclamationmark.triangle")
                    } else if javascriptBridge.stage == .noScript {
                        Label("No script loaded", systemImage: "questionmark")
                    } else {
                        if javascriptBridge.stage == .loadingBuiltin {
                            Label("Preparing...", systemImage: "clock")
                        } else if javascriptBridge.stage == .loadingScript {
                            Label("Loading...", systemImage: "clock")
                        } else if javascriptBridge.stage == .userInit {
                            Label("Initializing...", systemImage: "clock")
                        } else if javascriptBridge.stage == .bootingOrReset {
                            Label("Resetting...", systemImage: "clock")
                        } else if javascriptBridge.stage == .builtInLoaded {
                            /*
                             builtInLoaded is the transition stage between loadingBuiltin and loadingScript/noScript.
                             This period happens when the AppStage did not reach AppStageEnum.userScriptLoading
                             after AppStageEnum.stateReset yet. It should get there eventually though...
                             */
                            Label("Waiting for Homes to load...", systemImage: "clock")
                        } else {
                            Label("¯\\_(ツ)_/¯", systemImage: "questionmark.triangle")
                        }

                        Spacer()
                        ProgressView().progressViewStyle(.circular)
                    }
                }
                Button(
                    action: {
                        Threading.generalPurpose.runAsync {
                            UserScript.deleteCachedLibraries()
                        }
                    }, label: {
                        Label("Delete cached libraries", systemImage: "trash")
                    }
                ).disabled(userScript.isPlaceholder)
                Button(
                    action: {
                        Threading.generalPurpose.runAsync {
                            try? userScript.delete()
                            extensionURL = ""
                            AppStage.set(.stateReset)
                        }
                    }, label: {
                        Label("Delete user script", systemImage: "trash")
                    }
                )
                .foregroundColor(userScript.isPlaceholder ? .none : .red)
                .disabled(userScript.isPlaceholder && javascriptBridge.stage != .loadError)

                RestartJSButton()
            }

            Section(header: Text("User Scripts Parameters")) {
                if javascriptBridge.parameters.isEmpty {
                    Text("no parameters registered").disabled(true).padding()
                } else {
                    ForEach(Array(javascriptBridge.parameters.keys.sorted {
                        javascriptBridge.parameters[$0]!.displayName < javascriptBridge.parameters[$1]!.displayName
                    }), id: \.self) { key in
                        ParameterView(parameter: javascriptBridge.parameters[key]!)
                    }
                }

                Button(action: {
                    Threading.jsThread.runAsync {
                        for param in javascriptBridge.parameters {
                            param.value.resetToDefault()
                        }
                    }
                }, label: { Label("Reset to defaults", systemImage: "arrow.counterclockwise") })
            }.disabled(javascriptBridge.userScriptVersion == "")

            Section(header: Text("User Script Errors")) {
                ScrollView {
                    VStack {
                        if javascriptBridge.errors.isEmpty {
                            Text("none :)").disabled(true).padding()
                        } else {
                            ForEach(Array(javascriptBridge.errors.keys), id: \.self) { key in
                                Text((javascriptBridge.errors[key])!)
                            }
                        }
                    }
                }
            }
        }
        .navigationBarTitleDisplayMode(.inline)
        .navigationTitle("User Script")
//            .onChange(of: userScript.location, perform: {_ in})
    }
}

struct JavaScriptBridgeView_Previews: PreviewProvider {
    static var previews: some View {
        JavaScriptBridgeView(javascriptBridge: JavaScriptBridge.shared)
    }
}
