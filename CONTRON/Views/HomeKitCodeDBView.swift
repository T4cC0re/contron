//
//  HomeKitCodeDBView.swift
//  HomeKitCodeDBView
//
//  Created by Hendrik Meyer on 14.09.21.
//  Copyright © 2021 Hendrik Meyer. All rights reserved.
//

import SwiftUI

struct HomeKitCodeDBView: View {
    @ObservedObject var homekitCodeDB = HomeKitCodeDB.shared

    var body: some View {
//        Form {
        VStack {
            Text(
                "The QR code on the physical sticker might look different from these, but that is okay. The content is the same. It might just have been generated differently."
            )
            .fixedSize(horizontal: false, vertical: true)
            .font(.footnote)
            .foregroundColor(.gray)
            .multilineTextAlignment(.leading)
//                    .padding()
            List {
                ForEach(homekitCodeDB.codes, id: \.self) { entry in
                    VStack {
                        DisclosureGroup(entry.id) {
                            HomeKitStickerView(codeEntry: entry)
                        }
                    }
                }.onDelete(
                    perform: { offsets in
                        homekitCodeDB.codes.remove(atOffsets: offsets)
                        try? homekitCodeDB.save()
                    }
                )
            }
            .listStyle(.insetGrouped)
//            .onChange(of: homekitCodeDB.codes, perform: {_ in})
        }.navigationTitle("HomeKit Codes").navigationBarTitleDisplayMode(.inline)
    }
}

struct HomeKitCodeDBView_Previews: PreviewProvider {
    static var previews: some View {
        Text("mock")
    }
}
