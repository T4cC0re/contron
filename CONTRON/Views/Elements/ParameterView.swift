//
//  ParameterView.swift
//  ParameterView
//
//  Created by Hendrik Meyer on 30.08.21.
//  Copyright © 2021 Hendrik Meyer. All rights reserved.
//

import Combine
import HomeKit
import SwiftUI

struct ParameterView<T>: View where T: IParameter {
    var didChange = PassthroughSubject<Void, Never>()

    @ObservedObject var parameter: T
    @State var saving: Bool = false

    func valueWithUnitSuffix(value: Any, format: String) -> some View {
        guard format != "" else {
            return Text("")
        }
        guard (value as! Any?) != nil else {
            // Turns out `Any?` can be hard-cast to `Any` and passed into this...
            return Text("nil")
        }
        var text = ""

        if format == HMCharacteristicMetadataFormatString {
            text = (value as! String)
        } else if format == HMCharacteristicMetadataFormatInt {
            text = getBindingValue(Int.self, binding: (value as? Double) ?? Double(value as! Int)).description
        } else if format == HMCharacteristicMetadataFormatUInt8 {
            text = getBindingValue(UInt8.self, binding: (value as? Double) ?? Double(value as! UInt8)).description
        } else if format == HMCharacteristicMetadataFormatUInt16 {
            text = getBindingValue(UInt16.self, binding: (value as? Double) ?? Double(value as! UInt16)).description
        } else if format == HMCharacteristicMetadataFormatUInt32 {
            text = getBindingValue(UInt32.self, binding: (value as? Double) ?? Double(value as! UInt32)).description
        } else if format == HMCharacteristicMetadataFormatUInt64 {
            text = getBindingValue(UInt64.self, binding: (value as? Double) ?? Double(value as! UInt64)).description
        } else if format == HMCharacteristicMetadataFormatFloat {
            text = getBindingValue(Float.self, binding: (value as? Double) ?? Double(value as! Float)).description
        } else {
            text = (String(describing: parameter.value) + " type: \(format)")
        }

        guard let char = parameter as? HMCharJS else {
            return Text(text)
        }

        switch char.units {
        case HMCharacteristicMetadataUnitsPercentage:
            return Text("\(text) %")
        case HMCharacteristicMetadataUnitsPartsPerMillion:
            return Text("\(text) ppm")
        case HMCharacteristicMetadataUnitsCelsius:
            return Text("\(text) ºC")
        case HMCharacteristicMetadataUnitsFahrenheit:
            return Text("\(text) ºF")
        case HMCharacteristicMetadataUnitsSeconds:
            return Text("\(text) sec.")
        case HMCharacteristicMetadataUnitsLux:
            return Text("\(text) lx")
        case HMCharacteristicMetadataUnitsMicrogramsPerCubicMeter:
            return Text("\(text) µg/m3")
        case HMCharacteristicMetadataUnitsArcDegree:
            return Text("\(text) º")
        default:
            return Text(text)
        }
    }

    private func getBindingValue<T: BinaryInteger>(_: T.Type, binding: Double) -> T {
        return T(clamping: Int64(binding))
    }

    private func getBindingValue<T: BinaryFloatingPoint>(_: T.Type, binding: Double) -> T {
        return T(binding)
    }

    func slider<T: BinaryInteger>(_: T.Type, format _: String) -> some View {
        return HStack {
            let bind = Binding<Double>.init(
                get: {
                    Double(truncating: parameter.value as? NSNumber ?? 0)
                },
                set: {
                    val in
                    parameter.value = T(clamping: Int64(val))
                }
            )
            Slider(
                value: bind,
                in: (parameter.minVal ?? 0.0) ... (parameter.maxVal ?? 100.0),
                step: Double.Stride(parameter.step ?? 1)
            ) { editing in
                if !editing {
                    guard let char = parameter as? HMCharJS else {
                        return
                    }
                    saving = true
                    char.setInHomeKit(parameter.value) { val, error in
                        print("Setting was in HomeKit \(String(describing: val)) \(String(describing: error))")
                        saving = false
                    }
                }
            }
            .frame(maxWidth: .infinity, alignment: .leading)

            Stepper(
                "",
                value: bind,
                in: (parameter.minVal ?? 0.0) ... (parameter.maxVal ?? 100.0),
                step: Double.Stride(parameter.step ?? 0.5)
            ) { editing in
                if !editing {
                    guard let char = parameter as? HMCharJS else {
                        return
                    }
                    saving = true
                    char.setInHomeKit(parameter.value) { val, error in
                        print("Setting was in HomeKit \(String(describing: val)) \(String(describing: error))")
                        saving = false
                    }
                }
            }
            .frame(maxWidth: .infinity, alignment: .trailing)
        }
    }

    func slider<T: BinaryFloatingPoint>(_: T.Type, format _: String) -> some View {
        return HStack {
            let bind = Binding<Double>.init(
                get: {
                    Double(truncating: parameter.value as? NSNumber ?? 0)
                },
                set: {
                    val in
                    parameter.value = T(val)
                }
            )
            Slider(
                value: bind,
                in: (parameter.minVal ?? 0.0) ... (parameter.maxVal ?? 100.0),
                step: Double.Stride(parameter.step ?? 0.5)
            ) { editing in
                if !editing {
                    guard let char = parameter as? HMCharJS else {
                        return
                    }
                    saving = true
                    char.setInHomeKit(parameter.value) { val, error in
                        print("Setting was in HomeKit \(String(describing: val)) \(String(describing: error))")
                        saving = false
                    }
                }
            }
            .frame(maxWidth: .infinity, alignment: .leading)
            .padding(.trailing, -10)

            Stepper(
                "",
                value: bind,
                in: (parameter.minVal ?? 0.0) ... (parameter.maxVal ?? 100.0),
                step: Double.Stride(parameter.step ?? 0.5)
            ) { editing in
                if !editing {
                    guard let char = parameter as? HMCharJS else {
                        return
                    }
                    saving = true
                    char.setInHomeKit(parameter.value) { val, error in
                        print("Setting was in HomeKit \(String(describing: val)) \(String(describing: error))")
                        saving = false
                    }
                }
            }
        }
    }

    func fillFormat(_ format: String?) -> String {
        if format == nil {
            switch true {
            case parameter.value is Bool:
                return HMCharacteristicMetadataFormatBool
            case parameter.value is NSNumber:
                return HMCharacteristicMetadataFormatFloat
            case parameter.value is String:
                return HMCharacteristicMetadataFormatString
            default:
                return HMCharacteristicMetadataFormatData
            }
        }
        return format!
    }

    var body: some View {
        let format = fillFormat(parameter.format)

        VStack {
            HStack {
                Label(parameter.displayName, systemImage: parameter.icon)
                    .frame(maxWidth: .infinity, alignment: .leading)
                if parameter.isReadable {
                    if format == HMCharacteristicMetadataFormatBool {
                        if parameter.isWritable {
                            let bind = Binding<Bool>.init(
                                get: {
                                    parameter.value as? Bool ?? false
                                },
                                set: {
                                    val in
                                    parameter.value = val
                                    guard let char = parameter as? HMCharJS else {
                                        return
                                    }
                                    saving = true
                                    char.setInHomeKit(parameter.value) { val, error in
                                        print("Setting was in HomeKit \(String(describing: val)) \(String(describing: error))")
                                        saving = false
                                    }
                                }
                            )
                            Toggle(isOn: bind, label: {})
                        } else {
                            let bind = Binding<Bool>.init(
                                get: {
                                    parameter.value as? Bool ?? false
                                },
                                set: {
                                    _ in
                                }
                            )
                            Toggle(isOn: bind, label: {}).disabled(true)
                        }
                    } else if format != HMCharacteristicMetadataFormatString && format != HMCharacteristicMetadataFormatData && format != HMCharacteristicMetadataFormatTLV8 && format != HMCharacteristicMetadataFormatArray && format != HMCharacteristicMetadataFormatDictionary {
                        // We display a toogle already (either enabled or disabled) so we skip this
                        valueWithUnitSuffix(value: parameter.value, format: format)
                    }
                } else {
                    // TODO: Button for things
                    if parameter.isWritable {
                        Button(action: {
                            guard let char = parameter as? HMCharJS else {
                                return
                            }
                            saving = true
                            char.setInHomeKit(parameter.value) { val, error in
                                print("Setting was in HomeKit \(String(describing: val)) \(String(describing: error))")
                                saving = false
                            }
                        }, label: {
                            Image(systemName: "togglepower")
                        })
                    }
                }
                if saving || !((parameter as? HMCharJS)?.valueLoaded ?? true) {
                    Spacer()
                    ProgressView().progressViewStyle(.circular)
                }
            }
            if format != HMCharacteristicMetadataFormatBool {
                HStack {
                    // If we load a characteristic, we need to obey the valueLoaded state.
                    if !((parameter as? HMCharJS)?.valueLoaded ?? true) {
                        Text("loading")
                            .frame(maxWidth: .infinity, alignment: .center)
                    } else {
                        // TODO: Updated values for parameter are not reflected in UI!

                        if parameter.isWritable {
                            if format == HMCharacteristicMetadataFormatString {
                                valueWithUnitSuffix(value: parameter.value, format: format)
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                // TODO: Make editable
                            }
                            if format == HMCharacteristicMetadataFormatFloat {
                                slider(Float.self, format: format)
                            }
                            if format == HMCharacteristicMetadataFormatInt {
                                slider(Int.self, format: format)
                            }
                            if format == HMCharacteristicMetadataFormatUInt8 {
                                slider(UInt8.self, format: format)
                            }
                            if format == HMCharacteristicMetadataFormatUInt16 {
                                slider(UInt16.self, format: format)
                            }
                            if format == HMCharacteristicMetadataFormatUInt32 {
                                slider(UInt32.self, format: format)
                            }
                            if format == HMCharacteristicMetadataFormatUInt64 {
                                slider(UInt64.self, format: format)
                            }
                            //                if format == HMCharacteristicMetadataFormatData {
                            //                    //TODO
                            //                }
                            //                if format == HMCharacteristicMetadataFormatTLV8 {
                            //                    //TODO
                            //                }
                            //                if format == HMCharacteristicMetadataFormatArray {
                            //                    //TODO
                            //                }
                            //                if format == HMCharacteristicMetadataFormatDictionary {
                            //                    //TODO
                            //                }
                        } else {
                            if format == HMCharacteristicMetadataFormatString || format == HMCharacteristicMetadataFormatData || format == HMCharacteristicMetadataFormatTLV8 || format == HMCharacteristicMetadataFormatArray || format == HMCharacteristicMetadataFormatDictionary
                            {
                                valueWithUnitSuffix(value: parameter.value, format: format)
                                    .frame(maxWidth: .infinity, alignment: .trailing)
                            }
                        }
                    }
                }
                .padding(.top, 5)
            }
        }
        .padding(.top, 10)
        .padding(.bottom, 10)
    }
}

struct ParameterView_Previews: PreviewProvider {
    static var previews: some View {
        //        ParameterView()
        Text("Mock")
    }
}
