//
//  HomeKitAccessView.swift
//  HomeKitAccessView
//
//  Created by Hendrik Meyer on 03.09.21.
//  Copyright © 2021 Hendrik Meyer. All rights reserved.
//

import SwiftUI

struct HomeKitAccessView: View {
    @ObservedObject var homekitState = HomeKitState.shared

    var body: some View {
        Button(
            action: {
                #if targetEnvironment(macCatalyst)
                    UIApplication.shared.open(
                        URL(string: "x-apple.systempreferences:com.apple.preference.security?Privacy_HomeKit")!,
                        options: [:],
                        completionHandler: nil
                    )
                #else
                    UIApplication.shared.open(
                        URL(string: UIApplication.openSettingsURLString)!,
                        options: [:],
                        completionHandler: nil
                    )
                #endif
            },
            label: {
                if !homekitState.isAuthorized {
                    Label(
                        "HomeKit access denied",
                        systemImage: "xmark.seal"
                    ).foregroundColor(.red)
                } else {
                    Label("HomeKit access granted", systemImage: "checkmark.seal")
                }
            }
        )
    }
}

struct HomeKitAccessView_Previews: PreviewProvider {
    static var previews: some View {
        HomeKitAccessView()
    }
}
