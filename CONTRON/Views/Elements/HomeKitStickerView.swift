//
//  HomeKitStickerView.swift
//  HomeKitStickerView
//
//  Created by Hendrik Meyer on 11.09.21.
//  Copyright © 2021 Hendrik Meyer. All rights reserved.
//

import Foundation
import SwiftUI

struct HomeKitStickerView: View {
    @ObservedObject var codeEntry: HomeKitCodeEntry

    var body: some View {
        if codeEntry.setupURL != "" {
            ZStack {
                Color.white

                VStack {
                    HStack {
                        Image(systemName: "homekit")
                            .resizable().frame(width: 80, height: 70, alignment: .topLeading)
                        VStack {
                            Text(firstHalf(codeEntry.setupCode)!)
                            Text(secondHalf(codeEntry.setupCode)!)
                        }
                        .font(.system(size: 40, design: .monospaced))
                        .frame(alignment: .topTrailing)
                        //                            .padding(.all, 5)
                    }
                    .padding(.top, 5)
                    .padding(.bottom, -10)
                    Image(uiImage: QRCodeFrom(text: codeEntry.setupURL)!)
                        .resizable()
                        .frame(width: 200, height: 200, alignment: .top)
                        .padding(.bottom, 5)
                }
                .foregroundColor(.black)
            }
            .frame(width: 210, height: 300, alignment: .center)
            .frame(maxWidth: .infinity, alignment: .center)
        } else if codeEntry.setupCode != 0 {
            ZStack {
                Color.white
                Rectangle().border(.black, width: 2).padding(10)
                VStack {
                    Text(formatHKCode(codeEntry.setupCode) ?? "")
                        // TODO: Figure out how to make this scannable.
                        // Font used by Apple is Scancardium
                        .font(.system(size: 40, design: .rounded))
                }
                .foregroundColor(.black)
            }
            .frame(width: 290, height: 120, alignment: .center)
            .frame(maxWidth: .infinity, alignment: .center)

        } else {
            ZStack {
                VStack {
                    Text("No code")
                }
            }.frame(maxWidth: .infinity, alignment: .center)
        }
    }

    func firstHalf(_ input: UInt?) -> String? {
        guard let number = input else {
            return nil
        }
        let code = String(format: "%08u", number & 0x7FFFFFF)
        let start = code.startIndex
        let end = code.index(code.endIndex, offsetBy: -4)
        return String(code[start ..< end])
    }

    func secondHalf(_ input: UInt?) -> String? {
        guard let number = input else {
            return nil
        }
        let code = String(format: "%08u", number & 0x7FFFFFF)
        let start = code.index(code.startIndex, offsetBy: 4)
        let end = code.endIndex
        return String(code[start ..< end])
    }

    func formatHKCode(_ input: UInt?) -> String? {
        guard let number = input else {
            return nil
        }
        var code = String(format: "%08u", number & 0x7FFFFFF)
        code.insert("-", at: code.index(code.startIndex, offsetBy: 3))
        code.insert("-", at: code.index(code.startIndex, offsetBy: 6))
        return code
    }

    func QRCodeFrom(text: String) -> UIImage? {
        /**
         @see https://developer.apple.com/library/archive/documentation/GraphicsImaging/Reference/CoreImageFilterReference/index.html#//apple_ref/doc/filter/ci/CIQRCodeGenerator

         Generates an output image representing the input data according to the ISO/IEC 18004:2006 standard. The width and height of each module (square dot) of the code in the output image is one point. To create a QR code from a string or URL, convert it to an NSData object using the NSISOLatin1StringEncoding string encoding.
         */
        guard let filter = CIFilter(name: "CIQRCodeGenerator") else {
            return nil
        }
        let data = text.data(using: .isoLatin1, allowLossyConversion: false)
        filter.setValue(data, forKey: "inputMessage")
        filter.setValue("Q", forKey: "inputCorrectionLevel")
        guard let ciimage = filter.outputImage else {
            return nil
        }
        let transform = CGAffineTransform(scaleX: 10, y: 10)
        let scaledQrImage = ciimage.transformed(by: transform)
        let context = CIContext()
        guard let cgImage = context.createCGImage(scaledQrImage, from: scaledQrImage.extent) else { return nil }
        return UIImage(cgImage: cgImage)
    }
}

struct HomeKitStickerView_Previews: PreviewProvider {
    static var previews: some View {
        HomeKitStickerView(codeEntry: HomeKitCodeEntry("mock", setupURL: "X-HM://00A41JX65NZE9", setupCode: 12_345_678))
    }
}
