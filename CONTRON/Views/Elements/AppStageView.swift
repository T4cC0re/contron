//
//  AppStageView.swift
//  AppStageView
//
//  Created by Hendrik Meyer on 01.09.21.
//  Copyright © 2021 Hendrik Meyer. All rights reserved.
//

import SwiftUI

struct AppStageView: View {
    @ObservedObject var appStage = AppStage.shared
    var body: some View {
        HStack {
            switch appStage.stage {
            case .booting:
                Label("Starting up...", systemImage: "ellipsis")
            case .beforeAuthorization:
                Label("Waiting for HomeKit authorization...", systemImage: "homekit")
            case .afterAuthorization:
                Label("HomeKit access granted", systemImage: "homekit")
            case .stateReset:
                Label("Cleaning up...", systemImage: "wand.and.stars")
            case .homeKitLoading:
                Label("Loading Homes...", systemImage: "homekit")
            case .userScriptLoading:
                Label("User Script...", systemImage: "chevron.left.slash.chevron.right")
            case .ready:
                Label("Ready!", systemImage: "checkmark")
            }
            Spacer()
            if appStage.stage != .ready {
                ProgressView().progressViewStyle(.circular)
            }
        }
    }
}

struct AppStageView_Previews: PreviewProvider {
    static var previews: some View {
        AppStageView()
    }
}
