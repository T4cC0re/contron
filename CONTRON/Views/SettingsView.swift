//
//  SwiftUIView.swift
//  SwiftUIView
//
//  Created by Hendrik Meyer on 30.08.21.
//  Copyright © 2021 Hendrik Meyer. All rights reserved.
//

import SwiftUI

struct SettingsView: View {
    @AppStorage("show_unknown") var settingShowUnknown: Bool = UserDefaults.standard.bool(forKey: "show_unknown")
    @AppStorage("show_raw_data") var settingShowRawData: Bool = UserDefaults.standard.bool(forKey: "show_raw_data")
    @AppStorage("keep_screen_unlocked") var settingKeepScreenUnlocked: Bool = UserDefaults.standard.bool(forKey: "keep_screen_unlocked") {
        didSet {
            UIApplication.shared.isIdleTimerDisabled = settingKeepScreenUnlocked
        }
    }

    @AppStorage("save_in_icloud") var settingSaveIniCloud: Bool = FileIO.shared.isiCloudSupported

    #if targetEnvironment(macCatalyst)
        @AppStorage("start_minimized") var settingStartMinimized: Bool = UserDefaults.standard.bool(forKey: "start_minimized")
    #endif

    var body: some View {
        Form {
            Section(header: Text("HomeKit Access")) {
                HomeKitAccessView()
            }
            Section(header: Text("Settings")) {
                VStack(alignment: .leading) {
                    HStack {
                        Text("Save data in iCloud")
                        Toggle("", isOn: $settingSaveIniCloud).disabled(!FileIO.shared.isiCloudSupported)
                    }
                    Text("Data will not be transferred between device and iCloud").font(.footnote)
                }
                #if targetEnvironment(macCatalyst)
                    VStack(alignment: .leading) {
                        HStack {
                            Toggle("Start minimized", isOn: $settingStartMinimized)
                        }
                        Text("CONTRON will start minimized. You can open it via the menu bar.").font(.footnote)
                    }
                #endif
                DisclosureGroup(content: {
                    Toggle("Show unknown characteristics", isOn: $settingShowUnknown)
                    Toggle("Show raw data characteristics", isOn: $settingShowRawData)
                    Toggle("Prevent automatic screen-lock", isOn: $settingKeepScreenUnlocked)
                }, label: {
                    Label("Advanced", systemImage: "wrench.and.screwdriver")
                })
            }

        }.navigationBarTitleDisplayMode(.inline).navigationTitle("Settings")
    }
}

struct SettingsView_Previews: PreviewProvider {
    static var previews: some View {
        Text("")
    }
}
