//
//  HMAccView.swift
//  CONTRON
//
//  Created by Hendrik Meyer on 11.08.21.
//  Copyright © 2021 Hendrik Meyer. All rights reserved.
//

import SwiftUI

struct HMAccView: View {
    @ObservedObject var acc: HMAccJS

    var body: some View {
        Form {
            Section(
                content: {
                    if acc.isABridge || acc.isStandalone {
                        HStack {
                            Label("Type", systemImage: "homekit")
                            Spacer()
                            if acc.isABridge {
                                Text("bridge")
                            } else if acc.isStandalone {
                                Text("standalone")
                            }
                        }
                        NavigationLink(
                            destination: {
                                HomeKitCodeView(
                                    homeKitCodeEntry: HomeKitCodeDB.shared.findOrCreate(
                                        codeIdentifier: acc.homeKitCodeIdentifier
                                    )
                                )
                                .navigationBarTitleDisplayMode(.inline)
                                .navigationTitle("HomeKit code")
                            }, label: {
                                Label("HomeKit code", systemImage: "qrcode")
                            }
                        ).isDetailLink(true)
                    } else {
                        HStack {
                            Label("Type", systemImage: "link")
                            Spacer()
                            Text("bridged")
                        }
                    }
                },
                header: { Text("Details") }
            )
            List {
                ForEach(Array(acc.services.keys.sorted {
                    acc.services[$0]!.svc.isPrimaryService || acc.services[$0]!.name < acc.services[$1]!.name
                }), id: \.self) { key in
                    Section(
                        content: {
                            HMSvcView(svc: acc.services[key]!)
                        }, header: {
                            Label(acc.services[key]!.name, systemImage: "homekit")
                        }
                    )
                }
                if acc.services.isEmpty {
                    Label("No services", systemImage: "")
                }
            }.onChange(of: acc.services, perform: { _ in })
        }
    }
}

struct HMAccView_Previews: PreviewProvider {
    static var previews: some View {
        //        HMSView(s)
        Text("Mock")
    }
}
