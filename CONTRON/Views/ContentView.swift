//
//  ContentView.swift
//  CONTRON
//
//  Created by Hendrik Meyer on 25.02.20.
//  Copyright © 2020 Hendrik Meyer. All rights reserved.
//

import SwiftUI

var view = ContentView()

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

struct ContentView: View {
    @ObservedObject var jsBridge = JavaScriptBridge.shared
    @ObservedObject var homekitState = HomeKitState.shared

    var body: some View {
        NavigationView {
            VStack {
                Form {
                    Section(header: Text("Preferences"), content: {
                        NavigationLink(
                            destination: JavaScriptBridgeView(javascriptBridge: jsBridge)
                        ) {
                            Label("User Script", systemImage: "chevron.left.slash.chevron.right")
                        }

                        NavigationLink(
                            destination: SettingsView()

                        ) {
                            Label("Settings", systemImage: "gear")
                        }
                        NavigationLink(
                            destination: HomeKitCodeDBView()
                        ) {
                            Label("HomeKit Codes", systemImage: "qrcode")
                        }
                    })

                    Section(header: Text("Status"), content: {
                        AppStageView()
                    })

                    Section(header: Text("Homes"), content: {
                        if !homekitState.isAuthorized {
                            HomeKitAccessView()
                        } else if homekitState.homes.isEmpty {
                            Label("No Homes available", systemImage: "questionmark").disabled(true)
                        } else {
                            List {
                                ForEach(Array(homekitState.homes.keys.sorted {
                                    homekitState.homes[$0]!.name < homekitState.homes[$1]!.name
                                }), id: \.self) { uuid in
                                    NavigationLink(destination: {
                                        HMHomeView(homekitstate: homekitState, home: homekitState.homes[uuid]!)
                                    }) {
                                        Label(
                                            homekitState.homes[uuid]!.name,
                                            systemImage: homekitState.homes[uuid]!.home.isPrimary ? "house.fill" : "house"
                                        )
                                    }
                                }
                            }
                        }
                    }).onChange(of: homekitState.homes, perform: { _ in })

                    Section(header: Text("Quick access")) {
                        Button(action: JavaScriptBridge.shared.tickJS) {
                            Label("Tick JS", systemImage: "timer")
                        }
                    }
                }
                Divider()
                Text("Version \(Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "dev") - crafted with ♥").padding()
            }.navigationTitle("CONTRON").navigationBarTitleDisplayMode(.inline)
        }
    }
}
