//
//  HMCharView.swift
//  HMCharView
//
//  Created by Hendrik Meyer on 05.09.21.
//  Copyright © 2021 Hendrik Meyer. All rights reserved.
//

import HomeKit
import SwiftUI

struct HMCharView: View {
    @ObservedObject var characteristic: HMCharJS
    @AppStorage("show_unknown") var settingShowUnknown: Bool = UserDefaults.standard.bool(forKey: "show_unknown")
    @AppStorage("show_raw_data") var settingShowRawData: Bool = UserDefaults.standard.bool(forKey: "show_raw_data")

    // TODO: Changes do not yet set the characteristic
    var body: some View {
        if
            (characteristic.isKnown || settingShowUnknown)
            && ((characteristic.format != HMCharacteristicMetadataFormatData)
                || ((
                    characteristic.format == HMCharacteristicMetadataFormatData
                        || characteristic.format == HMCharacteristicMetadataFormatArray
                        || characteristic.format == HMCharacteristicMetadataFormatDictionary
                        || characteristic.format == HMCharacteristicMetadataFormatTLV8
                ) && settingShowRawData))
        {
            ParameterView(parameter: characteristic)
        }
    }
}

struct HMCharView_Previews: PreviewProvider {
    static var previews: some View {
        Text("nock")
    }
}
