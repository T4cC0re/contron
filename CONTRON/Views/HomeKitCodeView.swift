//
//  HomeKitCodeView.swift
//  HomeKitCodeView
//
//  Created by Hendrik Meyer on 11.09.21.
//  Copyright © 2021 Hendrik Meyer. All rights reserved.
//

import HomeKit
import SwiftUI
#if !targetEnvironment(macCatalyst)
    import CodeScanner
    import SlideOverCard
#endif

struct HomeKitCodeView: View {
    @State var scannerIsPresented: Bool = false
    @State var codeQuestionaireIsPresented: Bool = false
    @ObservedObject var homeKitCodeEntry: HomeKitCodeEntry

    var body: some View {
        ZStack {
            Form {
                HStack {
                    Label("Accessory Identifier", systemImage: "homekit")
                    Spacer()
                    Text(homeKitCodeEntry.id)
                }
                if homeKitCodeEntry.id.hasPrefix("unreachable-") {
                    Text("The accessory is unreachable. You can add a code, but it might not stay assigned to this accessory\nHowever, you will still be able to view it from the menu."
                    )
                    .fixedSize(horizontal: false, vertical: true)
                    .font(.footnote)
                    .foregroundColor(.red)
                    .multilineTextAlignment(.leading)
                }
                ZStack(alignment: .center) {
                    VStack {
                        HomeKitStickerView(codeEntry: homeKitCodeEntry)
                        if homeKitCodeEntry.setupURL != "" {
                            Text("The QR code on the physical sticker might look different from this, but that is okay. The content is the same. It might just have been generated differently."
                            )
                            .fixedSize(horizontal: false, vertical: true)
                            .font(.footnote)
                            .foregroundColor(.gray)
                            .multilineTextAlignment(.center)
                        }
                    }
                }

                #if !targetEnvironment(macCatalyst)
                    Button(action: {
                        scannerIsPresented = true
                    }, label: {
                        Text("Scan HomeKit QR Code")
                    }).disabled(homeKitCodeEntry.isNotEmpty)
                    Button(action: {
                        codeQuestionaireIsPresented = true
                    }, label: {
                        Text("Enter HomeKit pairing code manually")
                    }).disabled(homeKitCodeEntry.setupURL != "")
                    Button(action: {
                        homeKitCodeEntry.reset()
                    }, label: {
                        Text("Reset Code")
                            .foregroundColor(.red)
                    }).disabled(!homeKitCodeEntry.isNotEmpty)
                #endif
            }
            #if !targetEnvironment(macCatalyst)
                .slideOverCard(isPresented: $scannerIsPresented) {
                    CodeScannerView(codeTypes: [.qr], simulatedData: "X-HM://0015FHJYU") { result in
                        switch result {
                        case let .success(code):
                            print("Found code: \(code)")
                            scannerIsPresented = false
                            let setupCode = setupCodeFrom(uri: code)

                            if code != "" {
                                homeKitCodeEntry.setupURL = code
                            }
                            if setupCode != 0 {
                                homeKitCodeEntry.setupCode = setupCode
                            }

                            try? HomeKitCodeDB.shared.save(entity: homeKitCodeEntry)
                        case let .failure(error):
                            print(error.localizedDescription)
                            scannerIsPresented = false
                        }
                    }
                    .cornerRadius(15)
                    .frame(width: 200, height: 200)
                }
                .slideOverCard(
                    isPresented: $codeQuestionaireIsPresented,
                    options: [.disableDragToDismiss, .hideExitButton]
                ) {
                    VStack {
                        let bind = Binding<String>(get: {
                            if homeKitCodeEntry.setupCode == 0 {
                                return ""
                            }
                            return String(homeKitCodeEntry.setupCode)
                        }, set: {
                            val in
                            homeKitCodeEntry.setupCode = UInt(val) ?? 0
                        })

                        TextField(
                            "HomeKit Code",
                            text: bind
                        ) { _ in
                            //                       self.isEditing = isEditing
                        } onCommit: {
                            try? HomeKitCodeDB.shared.save(entity: homeKitCodeEntry)
                        }
                        .autocapitalization(.none)
                        .disableAutocorrection(true)
                        .keyboardType(.numberPad)
                        .border(Color(UIColor.separator))
                        .padding()
                        //                   Text(username)
                        //                       .foregroundColor(isEditing ? .red : .blue)
                        Button(
                            action: {
                                try? HomeKitCodeDB.shared.save(entity: homeKitCodeEntry)
                                codeQuestionaireIsPresented = false
                            },
                            label: {
                                Label("OK", systemImage: "checkmark")
                            }
                        )
                    }
                    .frame(height: 200, alignment: .center)
                }
            #endif
        }
    }

    @available(macCatalyst, unavailable)
    func setupCodeFrom(uri: String?) -> UInt {
        guard let uri = uri, uri.hasPrefix("X-HM://") else {
            return 0
        }

        let a = HMAccessorySetupPayload(url: URL(string: uri)!)

        let b = a?.value(forKey: "_internalSetupPayload")
        let setupNative = (b as? NSObject)?.value(forKey: "_setupCode")
        if (setupNative as? String) != nil {
            // We got a setup code :)
            // This will be in the format 123-45-678
            var setupString = setupNative as! String
            setupString = setupString.replacingOccurrences(of: "-", with: "")
            return UInt(setupString) ?? 0
        }
        return 0
    }
}

struct HomeKitCodeView_Previews: PreviewProvider {
    static var previews: some View {
        Text("mock")
    }
}
