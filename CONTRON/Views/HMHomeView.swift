//
//  HMHomeJSView.swift
//  HMHomeJSView
//
//  Created by Hendrik Meyer on 31.08.21.
//  Copyright © 2021 Hendrik Meyer. All rights reserved.
//

import SwiftUI

struct HMHomeView: View {
    @ObservedObject var homekitstate: HomeKitState
    @ObservedObject var home: HMHomeJS

    var body: some View {
        Form {
            Section(header: Text("Status")) {
                if home.accessories.isEmpty {
                    Label("No accessories available", systemImage: "xmark.seal").padding()
//                    } else if Int(state_characteristics) != 100 {
//                        ProgressView(value: state_characteristics, total: 100, label: {Label("Loading accessory characteristics", systemImage: "homekit").padding()})
                } else {
                    Label("Everything is good!", systemImage: "checkmark.circle").padding()
                }
            }

            if !homekitstate.isAuthorized {
                Text("Please allow HomeKit access in the system settings and restart the app.").foregroundColor(.red)
            } else {
                ForEach(Array(home.accessories.keys).sorted {
                    home.accessories[$0]!.name < home.accessories[$1]!.name
                }, id: \.self) { key in
                    NavigationLink(destination: {
                        HMAccView(acc: home.accessories[key]!)
                            .navigationBarTitleDisplayMode(.inline)
                            .navigationTitle(home.accessories[key]!.name)
                    }, label: {
                        if home.accessories[key]!.accessory.isBridged {
                            Label(home.accessories[key]!.name, systemImage: "link")
                        } else {
                            Label(home.accessories[key]!.name, systemImage: "homekit")
                        }
                    }).isDetailLink(true)
                }
            }
        }.navigationBarTitleDisplayMode(.inline).navigationTitle(home.name)
    }
}

struct HMHomeView_Previews: PreviewProvider {
    static var previews: some View {
        Text("mock")
    }
}
