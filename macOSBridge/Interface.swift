//
//  Interface.swift
//  CONTRON
//
//  Created by Hendrik Meyer on 10.08.21.
//  Copyright © 2021 Hendrik Meyer. All rights reserved.
//

import Foundation

@objc(macOSSpecific)
public protocol IMacOSSpecific: NSObjectProtocol {
    init()
    var catalyst: ICatalystCode? { get set }
    func createBarItem()
    func disableMaximizeButton()
}

@objc(catalystCode)
public protocol ICatalystCode: NSObjectProtocol {
    func barItemClicked()
    func newWindow()
}
