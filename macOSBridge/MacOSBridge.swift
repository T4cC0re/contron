//
//  MacOSBridge.swift
//  macOSBridge
//
//  Created by Hendrik Meyer on 10.08.21.
//  Copyright © 2021 Hendrik Meyer. All rights reserved.
//

import Foundation
import SwiftUI
#if os(macOS)
    import AppKit
#endif

#if os(macOS)
    final class MacOSBridge: NSObject, IMacOSSpecific, NSMenuDelegate {
        var catalyst: ICatalystCode?
        static var macSpecific: MacOSBridge!

        var statusBarItem: NSStatusItem!
        var menu: NSMenu!

        override init() {}

        func createBarItem() {
            let statusBar = NSStatusBar.system

            statusBarItem = statusBar.statusItem(withLength: NSStatusItem.variableLength)
            statusBarItem?.button?.title = "CONTRON"
            //        statusBarItem?.button?.action = #selector(MacOSBridge.togglePopover(_:))
            let statusBarMenu = NSMenu(title: "CONTRON Menu")
            statusBarMenu.addItem(withTitle: "Open CONTRON", action: #selector(open), keyEquivalent: "").target = self
            statusBarMenu.addItem(NSMenuItem.separator())
            statusBarMenu.addItem(withTitle: "Quit", action: #selector(quit), keyEquivalent: "").target = self
            menu = statusBarMenu
            statusBarItem.menu = menu
        }

        @objc func open() {
            catalyst?.newWindow()

            // Make sure we 'focus' the app
            NSApp.activate(ignoringOtherApps: true)

            for win in NSApplication.shared.windows {
                if win.windowController != nil {
                    // Make the window the top one
                    win.makeKeyAndOrderFront(self)
                }
            }
        }

        @objc func quit() {
            NSApplication.shared.terminate(self)
        }

        func disableMaximizeButton() {
            for win in NSApplication.shared.windows {
                // If we have a windowController it is a 'real' window
                if win.windowController != nil {
                    win.standardWindowButton(NSWindow.ButtonType.miniaturizeButton)?.isHidden = true
                    win.update()
                }
            }
        }
    }
#endif
